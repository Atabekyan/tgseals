<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 4/20/2019
 * Time: 11:00 PM
 */

return [
    'guest' => 'guest',
    'user' => 'user',
    'admin' => 'admin'
];