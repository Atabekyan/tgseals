<?php

namespace App\Exports;

use App\Models\StoreProduct;
use Maatwebsite\Excel\Concerns\FromCollection;

class StoreProductExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return StoreProduct::all();
    }
}
