<?php

namespace App\Imports;

use App\Models\Store;
use Maatwebsite\Excel\Concerns\ToModel;

class StoreImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Store([
            'id'=> $row[0],
            'product_group'=> $row[1],
            'type'=> $row[2],
            'compound'=> $row[3],
            'shore'=> $row[4],
            'special'=> $row[5],
            'color_specials'=> $row[6],
            'dimension'=> $row[7],
            'inner_min'=> $row[8],
            'inner_max'=> $row[9],
            'outer_min'=> $row[10],
            'outer_max'=> $row[11],
            'thickness_cross_section_min'=> $row[12],
            'thickness_cross_section_max'=> $row[13],
            'high_min'=> $row[14],
            'high_max'=> $row[15],
            'high_H_min'=> $row[16],
            'high_H_max'=> $row[17],
            'shaft_min'=> $row[18],
            'shaft_max'=> $row[19],
//            'created_at'=> $row[16],
//            'updated_at'=> $row[17],
        ]);
    }
}
