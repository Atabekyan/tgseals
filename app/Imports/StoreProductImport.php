<?php

namespace App\Imports;

use App\Models\StoreProduct;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class StoreProductImport implements ToCollection, WithChunkReading, ShouldQueue
{
    public function collection(Collection $rows)
    {
        $productsToInsert = [];

        foreach ($rows as $row)
        {
            array_push($productsToInsert, [
                'id'=> $row[0],
                'product_group'=> $row[1],
                'type'=> $row[2],
                'compound'=> $row[3],
                'shore'=> $row[4],
                'special'=> $row[5],
                'color_specials'=> $row[6],
                'dimension'=> $row[7],
                'inner'=> $row[8],
                'outer'=> $row[9],
                'thickness'=> $row[10],
                'high'=> $row[11],
                'high_H'=> $row[12],
                'shaft'=> $row[13],
                'price'=> $row[14],
                'quantity'=> $row[15],
                'is_available'=>$row[16],
                'created_at'=> $row[17],
                'updated_at'=> $row[18],
            ]);
        }

        StoreProduct::insert($productsToInsert);
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
