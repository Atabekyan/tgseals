<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 3/26/2019
 * Time: 3:26 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

/**
 * Class Locale
 * @package App\Http\Middleware
 * @author David Okhikyan <david.okhikyan@gmail.com>
 */
class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // get locale from session
        $locale = Session::get('locale');

        if ($locale){
            App::setLocale($locale);
        } else {
            App::setLocale(config('app.locale'));
        }

        return $next($request);
    }
}
