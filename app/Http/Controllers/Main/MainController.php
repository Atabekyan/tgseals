<?php

namespace App\Http\Controllers\Main;

use App\Models\Page;
use App\Models\PageImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main = Page::where(['page_name' => 'main'])->get();
        return view('main.main.index', compact('main'));
    }
    public function header()
    {
        return view('main.partials.header');
    }

    public function headerFirst()
    {
        return view('main.partials.header-first');
    }

    public function sidebar()
    {
        return view('main.partials.sidebar');
    }

    public function footer()
    {
        return view('main.partials.footer');
    }

    public function about_us()
    {
        $about_us = Page::where(['page_name' => 'about_us'])->get();
        $about_us_img = PageImages::where(['page_name' => 'about_us'])->get();
        return view('main.main.about-us',compact('about_us','about_us_img'));
    }
    public function materials()
    {
        $materials = Page::where(['page_name' => 'materials'])->get();
        $materials_img = PageImages::where(['page_name' => 'materials'])->get();
        return view('main.main.materials', compact('materials', 'materials_img'));
    }
    public function applications()
    {
        $applications = Page::where(['page_name' => 'applications'])->get();
        $applications_img = PageImages::where(['page_name' => 'applications'])->get();
        return view('main.main.applications', compact('applications', 'applications_img'));
    }
    public function compounds()
    {
        $compounds = Page::where(['page_name' => 'compounds'])->get();
        $compounds_img = PageImages::where(['page_name' => 'compounds'])->get();
        return view('main.main.compounds', compact('compounds', 'compounds_img'));
    }
    public function downloads()
    {
        return view('main.main.downloads');
    }

    public function privacy_policy()
    {
        $privacy_policy = Page::where(['page_name' => 'privacy_policy'])->get();
        $privacy_policy_img = PageImages::where(['page_name' => 'privacy_policy'])->get();
        return view('main.footer.privacy-policy', compact('privacy_policy', 'privacy_policy_img'));
    }
    public function imprint()
    {
        $imprint = Page::where(['page_name' => 'imprint'])->get();
        $imprint_img = PageImages::where(['page_name' => 'imprint'])->get();
        return view('main.footer.imprint', compact('imprint', 'imprint_img'));
    }

    public function e_shop()
    {
        return view('main.e-shop.index');
    }

    public function e_shop_info()
    {
        $e_shop = Page::where(['page_name' => 'e_shop'])->get();
        $e_shop_img = PageImages::where(['page_name' => 'e_shop'])->get();
        return view('main.e-shop.info',compact('e_shop','e_shop_img'));
    }

    public function changeLocale(Request $request){

        Session::put('locale' , $request->lang);
        return response('Lang was set successfully' , 200);
    }
}
