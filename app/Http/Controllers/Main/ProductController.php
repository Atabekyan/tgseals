<?php

namespace App\Http\Controllers\Main;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function products()
    {
        $main_category = Category::whereNull('parent_id')->get();
        $products = Product::all();
        return view('main.product.products' , compact('main_category','products'));
    }
}
