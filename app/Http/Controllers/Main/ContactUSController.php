<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Http\Requests;
use App\Models\ContactUS;
use Mail;

class ContactUSController extends Controller
{

    public function contacts()
    {
        $contact = Page::where(['page_name' => 'contact'])->get();
        return view('main.footer.contacts', compact('contact'));
    }
    /** * Show the application dashboard. * * @return \Illuminate\Http\Response */
    public function contactUSPost(Request $request)
    {

        $this->validate($request,[
            'name' => 'required',
            'company_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required',
            'topic' => 'required',
            'contact_form' => 'required' ],
            [
            'name.required' => ' The name field is required.',
            'company_name.required' => ' The company name field is required.',
        ]);
        ContactUS::create($request->all());

        Mail::send('email',
            array(
                'name' => $request->get('name'),
                'company_name' => $request->get('company_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'user_message' => $request->get('message'),
                'topic' => $request->get('topic'),
                'contact_form' => $request->get('contact_form'),
            ), function($message)
            {
                $message->from('tgseals.contactUS@gmail.com');
                $message->to('vt@tgseals.org', 'Admin')->subject('Contact us message from tgseals.com');
            });
        return back()->with('success', 'Thanks for contacting us!');
    }
}