<?php

namespace App\Http\Controllers\Main;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductListController extends Controller
{
    public function productList($category_id){

        $category = Category::find($category_id);
        if(!$category){
            abort('404');
        }

        $categories = [];
        array_push($categories , $category_id);

        if (count($category->subCategories)){
            foreach ($category->subCategorieS as $subcategory) {
                array_push($categories , $subcategory->id);
            }
        }
        $productsQuery = Product::whereIn('second_category_id' , $categories);
        $products = $productsQuery->get();

        return view('main.product.product-list' , compact('category' , 'products'));
    }
}
