<?php

namespace App\Http\Controllers\EShop;

use App\Models\Basket;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\StoreProduct;
use App\Models\Store;
use App\User;

class ProductController extends Controller
{

    public function index()
    {
        $user_id = auth()->user()->id;
        $products_for_search = Store::all();
        return response()->json(['user_id' => $user_id, 'products_for_search' => $products_for_search]);
    }

    public function basketIndex(){
        $user_id = auth()->user()->id;
        $basket = Basket::where('user_id', $user_id)->get();

        return response()->json(['user_id' => $user_id, 'basket' => $basket]);
    }

    public function basket (Request $request)
    {
        Basket::create([
            'user_id' => $request['user_id'],
            'product_group' => $request['product_group'],
            'type' => $request['type'],
            'compound' => $request['compound'],
            'shore' => $request['shore'],
            'special' => $request['special'],
            'color_specials' => $request['color_specials'],
            'dimension' => $request['dimension'],
            'thickness' => $request['thickness'],
            'outer' => $request['outer'],
            'high' => $request['high'],
            'quantity' => $request['quantity'],
            'is_available' => '0',
        ]);
    }

    public function deleteBasketOne(Request $request){
        Basket::destroy($request['0']);
    }

    public function deleteBasketAll(Request $request){
        foreach($request as $row => $innerArray) {
            foreach ($innerArray as $innerRow => $value) {
                Basket::destroy($value);
            }
        }
    }

//    public function basketAddOrderOne(Request $request){
//
//    }

    public function addBasketOrderAll(Request $request){

        foreach($request as $row => $innerArray){
            foreach($innerArray as $innerRow => $value){

                Order::create([
                    'user_id' => $value['user_id'],
                    'product_group' => $value['product_group'],
                    'type' => $value['type'],
                    'compound' => $value['compound'],
                    'shore' => $value['shore'],
                    'special' => $value['special'],
                    'color_specials' => $value['color_specials'],
                    'thickness' => $value['thickness'],
                    'outer' => $value['outer'],
                    'high' => $value['high'],
                    'high_H' => $value['high_H'],
                    'shaft' => $value['shaft'],
                    'price' => $value['price'],
                    'quantity' => $value['quantity']
                ]);

                Basket::destroy($value['id']);

            }
        }
    }

    public function orderIndex(){
        $user_id = auth()->user()->id;
        $order = Order::where('user_id', $user_id)->get();

        return response()->json(['order' => $order]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function order(Request $request){

        $order_in_stock = StoreProduct::where('product_group', $request['product_group'])
                                ->where('type', $request['type'])
                                ->where('compound', $request['compound'])
                                ->where('shore', $request['shore'])
                                ->where('special',$request['special'])
                                ->where('color_specials', $request['color_specials'])
                                ->where('inner', $request['inner'])
                                ->where('outer', $request['outer'])
                                ->where('thickness', $request['thickness'])
                                ->where('high', $request['high'])
                                ->where('high_H', $request['high_H'])
                                ->where('shaft', $request['shaft'])
                                ->first();
        $user_quantity = $request['quantity'];
        $quantity = false;
        if(is_null($order_in_stock)){
            $order_out_stock = $request->all();
        }else{
            if($order_in_stock['quantity'] >= $request['quantity']){
                $quantity = true ;
              
            }
            $order_out_stock = null;
        }

        return response()->json(['order_in_stock' => $order_in_stock, 'order_out_stock' => $order_out_stock, 'quantity' => $quantity, 'user_quantity' => $user_quantity]);
    }


    public function orderSubmit (Request $request)
    {
        Order::create([
            'user_id' => $request['user_id'],
            'product_group' => $request['product_group'],
            'type' => $request['type'],
            'compound' => $request['compound'],
            'shore' => $request['shore'],
            'special' => $request['special'],
            'color_specials' => $request['color_specials'],
            'thickness' => $request['thickness'],
            'outer' => $request['outer'],
            'high' => $request['high'],
            'high_H' => $request['high_H'],
            'shaft' => $request['shaft'],
            'price' => $request['price'],
            'quantity' => $request['user_quantity']
        ]);

        if(isset($request['id'])){
            Basket::destroy($request['id']);
        }

    }




}