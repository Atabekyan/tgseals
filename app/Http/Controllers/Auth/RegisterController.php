<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'company_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'surname' => $data['surname'],
            'company_name' => $data['company_name'],
            'phone' => $data['phone'],
            'vat' => $data['vat'],
            'address' => $data['address'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => config('user_roles')['user']
        ]);
    }
//    public function createUser(Request $request)
//    {
//        dd($request->all());
//        if( !count($this->validator($request->all())->messages()) ){
//
//            $this->create($request->all());
//        }
//
//        Mail::send('user_email',
//            array(
//                'name' => $request->get('name'),
//                'company_name' => $request->get('company_name'),
//                'email' => $request->get('email'),
//                'phone' => $request->get('phone'),
//                'role' => $request->get('role'),
//
//            ), function($message)
//            {
//                $message->from('tgseals.contactUS@gmail.com');
//                $message->to('artak.atabekyan94@gmail.com', 'Admin')->subject('User request from tgseals.com');
//            });
//        return redirect()->back();
//    }
}
