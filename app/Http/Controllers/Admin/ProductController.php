<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\ProductSettings;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_category = Category::whereNull('parent_id')->get();
        return view('admin.product.create', compact('main_category'));
    }

    public function createSettings($id)
    {
        $product = Product::find($id);
        return view('admin.product.create_settings', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get request params
        $requestData = $request->all();

        // upload the product type img
        if ($request->has('type_img')){
            $imgName = time().'.'.$request->type_img->getClientOriginalExtension();
            $request->type_img->move(public_path('images/product/type'), $imgName);
            $requestData['type_img'] = $imgName;
        }

        $product = true;
        while ($product){
            $random_string = $this->generateRandomString(10);
            $product = Product::where('unique_id',$random_string)
                ->first();
        }
        $requestData['unique_id'] = $random_string;

        $product = Product::create($requestData);

        // add images to product
//        if($request->images){
//            $images = explode(',' , $request->images);
//
//            foreach ($images as $image) {
//
//                ProductImages::create([
//                    'product_id' => $product->id,
//                    'url' => $image
//                ]);
//            }
//        }

        return redirect()->route('product.getIndex');
    }

    public function storeSettings(Request $request)
    {
        $request['is_available'] == 'on' ? $request['is_available'] = 1 : $request['is_available'] = 0;

        // get request params
        $requestData = $request->all();

        $product_settings = ProductSettings::create($requestData);

        return redirect()->route('product.getIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $main_category = Category::whereNull('parent_id')->get();

        return view('admin.product.edit', compact('product', 'main_category'));
    }

    public function editSettings($id)
    {
        $product_settings = ProductSettings::find($id);
        $product = Product::find($product_settings['product_id']);
//        dd($product_settings , $product);
//        dd($product);

        return view('admin.product.edit_settings', compact('product', 'product_settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // get request params
        $requestData = $request->all();

        // get product
        $model = Product::find($request->id);

        // upload the product type img
        if ($request->has('type_img')){
            $imgName = time().'.'.$request->type_img->getClientOriginalExtension();
            $request->type_img->move(public_path('images/product/type'), $imgName);
            $requestData['type_img'] = $imgName;
        }

        // fill model with updated data
        $model -> fill($requestData);

        // save
        $model -> save();

        return redirect()->route('product.getIndex');
    }

    public function updateSettings(Request $request)
    {
        $request['is_available'] == 'on' ? $request['is_available'] = 1 : $request['is_available'] = 0;

        $model = ProductSettings::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('product.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        ProductSettings::where('product_id' , $id)->delete();

        return redirect()->back();
    }

    public function destroySettings($id)
    {
        ProductSettings::destroy($id);
        return redirect()->back();
    }
    /*
     * Generate Unique Id
     * */
    function generateRandomString($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
