<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_category = Category::whereNull('parent_id')->get();

        return view('admin.main-category.index',
            compact('main_category')
        );
    }

    public function indexSecondCategory()
    {
        $main_category = Category::whereNull('parent_id')->get();

        return view('admin.second-category.index',
            compact('main_category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMainCategory()
    {
        return view('admin.main-category.create');
    }

    public function createSecondCategory()
    {
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.second-category.create', compact('categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function storeMainCategory(Request $request){
        Category::create($request->all());
        return redirect()->route('main-category.getIndex');
    }

    public function storeSecondCategory(Request $request)
    {
        // get request params
        $requestData = $request->all();

        // upload the category icon
        if ($request->has('img')){
            $imgName = time().'.'.$request->img->getClientOriginalExtension();
            $request->img->move(public_path('images/category/images'), $imgName);
            $requestData['img'] = $imgName;
        }
        // create category
        $category = Category::create($requestData);

        return redirect()->route('second-category.getIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function editMainCategory($id)
    {
        $main_category = Category::find($id);

        return view('admin.main-category.edit', compact('main_category'));
    }
    public function editSecondCategory($id)
    {
        $second_category = Category::find($id);
        $main_category = Category::whereNull('parent_id')->get();

        return view('admin.second-category.edit', compact('second_category', 'main_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function updateMainCategory(Request $request)
    {
        $model = Category::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('main-category.getIndex');
    }
    public function updateSecondCategory(Request $request)
    {
        // get request params
        $requestData = $request->all();

        // get category
        $model = Category::find($request->id);

        // upload the category icon
        if ($request->has('img')){
            $imgName = time().'.'.$request->img->getClientOriginalExtension();
            $request->img->move(public_path('images/category/images'), $imgName);
            $requestData['img'] = $imgName;
        }

        // fill model with updated data
        $model->fill($requestData);

        // save
        $model->save();

        return redirect()->route('second-category.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        Category::where('parent_id' , $id)->delete();
        Product::where('second_category_id' , $id)->delete();

        return redirect()->back();
    }
}
