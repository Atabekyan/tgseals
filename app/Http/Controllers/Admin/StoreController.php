<?php

namespace App\Http\Controllers\Admin;

use App\Models\Store;
use App\Models\StoreProduct;
use Illuminate\Http\Request;
use App\Exports\StoreExport;
use App\Imports\StoreImport;
use App\Exports\StoreProductExport;
use App\Imports\StoreProductImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {
        $store = Store::all();

        return view('admin.store.store',['store'=>$store]);
    }
    public function importExportViewProduct()
    {
        $products = StoreProduct::paginate(100);

        return view('admin.store.store_product',['products'=>$products]);
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new StoreExport, 'Store_For_Search.xlsx');
    }

    public function exportProduct()
    {
        return Excel::download(new StoreProductExport, 'StoreProduct_In_Stock.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import()
    {
        Store::query()->truncate();
        Excel::import(new StoreImport,request()->file('file'));

        return back();
    }

    public function importProduct()
    {
        StoreProduct::query()->truncate();
        Excel::queueImport(new StoreProductImport,request()->file('file'));

        return back();
    }
}
