<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\PageImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    //About Us Page
    public function indexAboutUS()
    {
        $about_us = Page::where(['page_name' => 'about_us'])->get();
        $about_us_img = PageImages::where(['page_name' => 'about_us'])->get();

        return view('admin.pages.about-us.index', compact('about_us', 'about_us_img'));
    }

//    Create Img
    public function createAboutUsImg(){
        return view('admin.pages.about-us.create-img');
    }
    public function storeAboutUsImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }

        PageImages::create($requestData);

        return redirect()->route('about-us.getIndex');
    }

//    Create Header
    public function createAboutUsHeader()
    {
        return view('admin.pages.about-us.create-header');
    }
    public function storeAboutUsHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('about-us.getIndex');
    }
//    Create Content
    public function createAboutUsContent()
    {
        return view('admin.pages.about-us.create-content');
    }
    public function storeAboutUsContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('about-us.getIndex');
    }
//    Edit Header
    public function editAboutUsHeader($id)
    {
        $about_us = Page::find($id);

        return view('admin.pages.about-us.edit-header', compact('about_us'));
    }
    public function updateAboutUsHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('about-us.getIndex');
    }
//    Edit Content
    public function editAboutUsContent($id)
    {
        $about_us = Page::find($id);

        return view('admin.pages.about-us.edit-content', compact('about_us'));
    }
    public function updateAboutUsContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('about-us.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAboutUs($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }


//    Applications Page
    public function indexApplications()
    {
        $applications = Page::where(['page_name' => 'applications'])->get();
        $applications_img = PageImages::where(['page_name' => 'applications'])->get();

        return view('admin.pages.applications.index', compact('applications', 'applications_img'));

    }
    //    Create Img
    public function createApplicationsImg(){
        return view('admin.pages.applications.create-img');
    }
    public function storeApplicationsImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }
//        dd($requestData);
        PageImages::create($requestData);


        return redirect()->route('applications.getIndex');
    }
//    Create Header
    public function createApplicationsHeader()
    {
        return view('admin.pages.applications.create-header');
    }
    public function storeApplicationsHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('applications.getIndex');
    }
//    Create Content
    public function createApplicationsContent()
    {
        return view('admin.pages.applications.create-content');
    }
    public function storeApplicationsContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('applications.getIndex');
    }
//    Edit Header
    public function editApplicationsHeader($id)
    {
        $applications = Page::find($id);

        return view('admin.pages.applications.edit-header', compact('applications'));
    }
    public function updateApplicationsHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('applications.getIndex');
    }
//    Edit Content
    public function editApplicationsContent($id)
    {
        $applications = Page::find($id);

        return view('admin.pages.applications.edit-content', compact('applications'));
    }
    public function updateApplicationsContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('applications.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyApplications($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }

    //Compounds Page
    public function indexCompounds()
    {
        $compounds = Page::where(['page_name' => 'compounds'])->get();
        $compounds_img = PageImages::where(['page_name' => 'compounds'])->get();
        return view('admin.pages.compounds.index', compact('compounds', 'compounds_img'));
    }
    //    Create Img
    public function createCompoundsImg(){
        return view('admin.pages.compounds.create-img');
    }
    public function storeCompoundsImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }
        PageImages::create($requestData);

        return redirect()->route('compounds.getIndex');
    }
//    Create Header
    public function createCompoundsHeader()
    {
        return view('admin.pages.compounds.create-header');
    }
    public function storeCompoundsHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('compounds.getIndex');
    }
//    Create Content
    public function createCompoundsContent()
    {
        return view('admin.pages.compounds.create-content');
    }
    public function storeCompoundsContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('compounds.getIndex');
    }
//    Edit Header
    public function editCompoundsHeader($id)
    {
        $compounds = Page::find($id);

        return view('admin.pages.compounds.edit-header', compact('compounds'));
    }
    public function updateCompoundsHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('compounds.getIndex');
    }
//    Edit Content
    public function editCompoundsContent($id)
    {
        $compounds = Page::find($id);

        return view('admin.pages.compounds.edit-content', compact('compounds'));
    }
    public function updateCompoundsContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('compounds.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCompounds($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }


    //Contact Page
    public function indexContact()
    {
        $contact = Page::where(['page_name' => 'contact'])->get();
        return view('admin.pages.contact.index', compact('contact'));
    }

//    Create Header
    public function createContactHeader()
    {
        return view('admin.pages.contact.create-header');
    }
    public function storeContactHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('contact.getIndex');
    }
//    Create Content
    public function createContactContent()
    {
        return view('admin.pages.contact.create-content');
    }
    public function storeContactContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('contact.getIndex');
    }
//    Edit Header
    public function editContactHeader($id)
    {
        $contact = Page::find($id);

        return view('admin.pages.contact.edit-header', compact('contact'));
    }
    public function updateContactHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('contact.getIndex');
    }
//    Edit Content
    public function editContactContent($id)
    {
        $contact = Page::find($id);

        return view('admin.pages.contact.edit-content', compact('contact'));
    }
    public function updateContactContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('contact.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyContact($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }


    //Imprint Page
    public function indexImprint()
    {
        $imprint = Page::where(['page_name' => 'imprint'])->get();
        $imprint_img = PageImages::where(['page_name' => 'imprint'])->get();

        return view('admin.pages.imprint.index', compact('imprint', 'imprint_img'));
    }
    //    Create Img
    public function createImprintImg(){
        return view('admin.pages.imprint.create-img');
    }
    public function storeImprintImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }
        PageImages::create($requestData);

        return redirect()->route('imprint.getIndex');
    }
//    Create Header
    public function createImprintHeader()
    {
        return view('admin.pages.imprint.create-header');
    }
    public function storeImprintHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('imprint.getIndex');
    }
//    Create Content
    public function createImprintContent()
    {
        return view('admin.pages.imprint.create-content');
    }
    public function storeImprintContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('imprint.getIndex');
    }
//    Edit Header
    public function editImprintHeader($id)
    {
        $imprint = Page::find($id);

        return view('admin.pages.imprint.edit-header', compact('imprint'));
    }
    public function updateImprintHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('imprint.getIndex');
    }
//    Edit Content
    public function editImprintContent($id)
    {
        $imprint = Page::find($id);

        return view('admin.pages.imprint.edit-content', compact('imprint'));
    }
    public function updateImprintContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('imprint.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyImprint($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }

    //Materials Page
    public function indexMaterials()
    {
        $materials = Page::where(['page_name' => 'materials'])->get();
        $materials_img = PageImages::where(['page_name' => 'materials'])->get();

        return view('admin.pages.materials.index', compact('materials', 'materials_img'));

    }
    //    Create Img
    public function createMaterialsImg(){
        return view('admin.pages.materials.create-img');
    }
    public function storeMaterialsImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }
        PageImages::create($requestData);

        return redirect()->route('materials.getIndex');
    }
//    Create Header
    public function createMaterialsHeader()
    {
        return view('admin.pages.materials.create-header');
    }
    public function storeMaterialsHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('materials.getIndex');
    }
//    Create Content
    public function createMaterialsContent()
    {
        return view('admin.pages.materials.create-content');
    }
    public function storeMaterialsContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('materials.getIndex');
    }
//    Edit Header
    public function editMaterialsHeader($id)
    {
        $materials = Page::find($id);

        return view('admin.pages.materials.edit-header', compact('materials'));
    }
    public function updateMaterialsHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('materials.getIndex');
    }
//    Edit Content
    public function editMaterialsContent($id)
    {
        $materials = Page::find($id);

        return view('admin.pages.materials.edit-content', compact('materials'));
    }
    public function updateMaterialsContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('materials.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMaterials($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }


    //Privacy Policy Page
    public function indexPrivacyPolicy()
    {
        $privacy_policy = Page::where(['page_name' => 'privacy_policy'])->get();
        $privacy_policy_img = PageImages::where(['page_name' => 'privacy_policy'])->get();

        return view('admin.pages.privacy-policy.index', compact('privacy_policy', 'privacy_policy_img'));
    }
    //    Create Img
    public function createPrivacyPolicyImg(){
        return view('admin.pages.privacy-policy.create-img');
    }
    public function storePrivacyPolicyImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }
        PageImages::create($requestData);

        return redirect()->route('privacy-policy.getIndex');
    }
//    Create Header
    public function createPrivacyPolicyHeader()
    {
        return view('admin.pages.privacy-policy.create-header');
    }
    public function storePrivacyPolicyHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('privacy-policy.getIndex');
    }
//    Create Content
    public function createPrivacyPolicyContent()
    {
        return view('admin.pages.privacy-policy.create-content');
    }
    public function storePrivacyPolicyContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('privacy-policy.getIndex');
    }
//    Edit Header
    public function editPrivacyPolicyHeader($id)
    {
        $privacy_policy = Page::find($id);

        return view('admin.pages.privacy-policy.edit-header', compact('privacy_policy'));
    }
    public function updatePrivacyPolicyHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('privacy-policy.getIndex');
    }
//    Edit Content
    public function editPrivacyPolicyContent($id)
    {
        $privacy_policy = Page::find($id);

        return view('admin.pages.privacy-policy.edit-content', compact('privacy_policy'));
    }
    public function updatePrivacyPolicyContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('privacy-policy.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPrivacyPolicy($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }


    //Main Page
    public function indexMain()
    {
        $main = Page::where(['page_name' => 'main'])->get();

        return view('admin.pages.main.index', compact('main'));

    }
//    Create Header
    public function createMainHeader()
    {
        return view('admin.pages.main.create-header');
    }
    public function storeMainHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('main-admin.getIndex');
    }
//    Create Content
    public function createMainContent()
    {
        return view('admin.pages.main.create-content');
    }
    public function storeMainContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('main-admin.getIndex');
    }
//    Edit Header
    public function editMainHeader($id)
    {
        $main = Page::find($id);

        return view('admin.pages.main.edit-header', compact('main'));
    }
    public function updateMainHeader(Request $request, $id)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('main-admin.getIndex');
    }
//    Edit Content
    public function editMainContent($id)
    {
        $main = Page::find($id);

        return view('admin.pages.main.edit-content', compact('main'));
    }
    public function updateMainContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('main-admin.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMain($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }

    //E-Shop Page
    public function indexEShop()
    {
        $e_shop = Page::where(['page_name' => 'e_shop'])->get();
        $e_shop_img = PageImages::where(['page_name' => 'e_shop'])->get();

        return view('admin.pages.e-shop.index', compact('e_shop', 'e_shop_img'));
    }

//    Create Img
    public function createEShopImg(){
        return view('admin.pages.e-shop.create-img');
    }
    public function storeEShopImg(Request $request)
    {
        // get request params
        $requestData = $request->all();

        if ($request->has('url')){
            $imgName = time().'.'.$request->url->getClientOriginalExtension();
            $request->url->move(public_path('images/pages'), $imgName);
            $requestData['url'] = $imgName;
        }

        PageImages::create($requestData);

        return redirect()->route('e-shop.getIndex');
    }

//    Create Header
    public function createEShopHeader()
    {
        return view('admin.pages.e-shop.create-header');
    }
    public function storeEShopHeader(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('e-shop.getIndex');
    }
//    Create Content
    public function createEShopContent()
    {
        return view('admin.pages.e-shop.create-content');
    }
    public function storeEShopContent(Request $request)
    {
        Page::create($request->all());
        return redirect()->route('e-shop.getIndex');
    }
//    Edit Header
    public function editEShopHeader($id)
    {
        $e_shop = Page::find($id);

        return view('admin.pages.e-shop.edit-header', compact('e_shop'));
    }
    public function updateEShopHeader(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('e-shop.getIndex');
    }
//    Edit Content
    public function editEShopContent($id)
    {
        $e_shop = Page::find($id);

        return view('admin.pages.e-shop.edit-content', compact('e_shop'));
    }
    public function updateEShopContent(Request $request)
    {
        $model = Page::find($request->id);
        $model -> fill($request->all());
        $model -> save();

        return redirect()->route('e-shop.getIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyEShop($id)
    {
        Page::destroy($id);
        return redirect()->back();
    }

    public function destroyImg($id)
    {
        PageImages::destroy($id);
        return redirect()->back();
    }

}
