<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'name_ru', 'name_am' , 'name_en' ,'img', 'order'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories(){
        return $this->hasMany(Category::class , 'parent_id' , 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parentCategory(){
        return $this->hasOne(Category::class , 'id' , 'parent_id');
    }
}
