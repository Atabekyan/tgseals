<?php

namespace App\Models;

use App\Traits\FullTextSearchTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type_img', 'name_ru', 'name_am', 'name_en',
        'type','main_category_id', 'second_category_id', 'img',
        'description_ru' ,'description_en', 'description_am', 'main_category', 'second_category', 'unique_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $searchable = [
        'name_ru',
        'name_en',
        'name_am'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories(){
        return $this->hasMany(Category::class , 'parent_id' , 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function main_category_id(){
        return $this->hasOne(Category::class , 'id' , 'second_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images(){
        return $this->hasMany(ProductImages::class , 'product_id' , 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function optionalFields(){
        return $this->hasMany(ProductOptionalField::class , 'product_id' , 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function favorites(){
        return $this->hasMany(Favourite::class , 'product_id' , 'id');
    }

    public function product_settings(){
        return $this->hasMany( ProductSettings::class , 'product_id' , 'id');
    }


}