<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'store_for_search';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_group', 'type',  'compound', 'shore', 'special', 'color_specials',
        'dimension','inner_min','inner_max', 'outer_min', 'outer_max', 'thickness_cross_section_min',
        'thickness_cross_section_max', 'high_min', 'high_max', 'high_H_min', 'high_H_max', 'shaft_min', 'shaft_max'];


    protected $dates = ['created_at', 'updated_at'];
}
