<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreProduct extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'store_products_in_stock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_group', 'type',  'compound', 'shore', 'special', 'color_specials', 'dimension',
        'inner','outer', 'thickness', 'high', 'high_H', 'shaft', 'price', 'quantity','is_available'];

    protected $dates = ['created_at', 'updated_at'];
}
