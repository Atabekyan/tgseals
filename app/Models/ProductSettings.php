<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSettings extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'products_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'material_ru', 'material_am', 'material_en',
        'temp_min', 'temp_max', 'speed', 'pressure', 'dimensions',
         'quantity', 'is_available'];

    protected $dates = ['created_at', 'updated_at'];

    protected $searchable = [
        'name_ru',
        'name_en',
        'name_am'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

//    public function product_settings(){
//        return $this->hasOne( Product::class , 'id' , 'product_id');
//    }

    public function subcategories(){
        return $this->hasMany(Category::class , 'parent_id' , 'id');
    }

    public function category(){
        return $this->hasOne(Category::class , 'id' , 'main_category_id');
    }
}
