<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'product_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['product_id', 'url'];

    protected $dates = ['created_at', 'updated_at'];
}
