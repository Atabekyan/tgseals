<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    /*
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'basket';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'product_group', 'type',  'compound', 'shore', 'special', 'color_specials', 'dimension',
        'inner','outer', 'thickness', 'high', 'price', 'quantity','is_available'];

    protected $dates = ['created_at', 'updated_at'];
}
