<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageImages extends Model
{
    /*
   * The database table used by the model.
   *
   * @var string
   */
    protected $table = 'page_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['page_name', 'url'];

    protected $dates = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function subCategories(){
//        return $this->hasMany(Category::class , 'parent_id' , 'id');
//    }
//
//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\HasOne
//     */
//    public function parentCategory(){
//        return $this->hasOne(Category::class , 'id' , 'parent_id');
//    }
}
