{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 27-Feb-19--}}
{{--* Time: 3:50 PM--}}
{{--*/--}}


<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu"
                    aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('admin.getIndex') }}"><img src="../../images/main_logo.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="{{ route('admin.getIndex') }}"></a>
        </div>
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('admin.getIndex') }}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">Administration Part</h3><!-- /.menu-title -->

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon ti-layout-grid3"></i>Categories </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa  fa-circle"></i><a href="{{ route('main-category.getIndex') }}">Main category</a></li>
                        <li><i class="fa fa-dot-circle-o"></i><a href="{{ route('second-category.getIndex') }}">Second category</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('product.getIndex') }}"><i class="menu-icon ti-shopping-cart-full"></i>Products </a>
                </li>
                <li>
                    <a href="{{ route('user.getIndex') }}"> <i class="menu-icon ti-user"></i>Users</a>
                </li>
                <li>
                    <a href="{{ route('basket.getIndex') }}"> <i class="menu-icon ti-shopping-cart-full"></i>Basket</a>
                </li>
                <li>
                    <a href="{{ route('order.getIndex') }}"> <i class="menu-icon ti-shopping-cart-full"></i>Orders</a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('main-admin.getIndex') }}">Main</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('about-us.getIndex') }}">About Us</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('applications.getIndex') }}">Applications</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('compounds.getIndex') }}">Compounds</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('contact.getIndex') }}">Contact</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('imprint.getIndex') }}">Imprint</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('materials.getIndex') }}">Materials</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('privacy-policy.getIndex') }}">Privacy Policy</a></li>
                        <li><i class="menu-icon fa fa-sign-in"></i><a href="{{ route('e-shop.getIndex') }}">E-Shop</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('messages.getIndex') }}"> <i class="menu-icon ti-email"></i>Messages</a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-barcode"></i>E-Shop
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa fa-search"></i><a href="{{ route('store.storeForSearch') }}">Store For Search</a></li>
                        <li><i class="fa ti-shopping-cart-full"></i><a href="{{ route('store.storeProduct') }}">Store Product In Stock</a></li>
                    </ul>
                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->