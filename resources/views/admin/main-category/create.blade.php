{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 28-Feb-19--}}
{{--* Time: 6:00 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Categories</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Categories</a></li>
                        <li><a href="{{ route('main-category.getIndex') }}">Main categories</a></li>
                        <li class="active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Main Categories</strong> create
                </div>
                <form id="add-main-category-form" method="POST" action="{{ route('main-category.postCreate') }}">
                    {{ csrf_field() }}
                    <div class="form-group card-body card-block">
                        <div class="form-group">
                            <label class="form-control-label">Input English Name</label>
                            <input type="text" name="name_en" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Name</label>
                            <input type="text" name="name_am" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Name</label>
                            <input type="text" name="name_ru" class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>


    </div>




@endsection