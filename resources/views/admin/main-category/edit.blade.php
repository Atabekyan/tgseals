{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 28-Feb-19--}}
 {{--* Time: 6:01 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Categories</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Categories</a></li>
                        <li><a href="{{ route('main-category.getIndex') }}">Main categories</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Main Categories</strong> update ({{$main_category->name_en}})
                </div>
                <form id="edit-main-category-form" method="POST" action="{{ route('main-category.postUpdate') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $main_category->id }}">
                    <div class="form-group card-body card-block">
                        <div class="form-group">
                            <label class="form-control-label">Input English Name</label>
                            <input type="text" name="name_en" value="{{ $main_category->name_en }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Name</label>
                            <input type="text" name="name_am" value="{{ $main_category->name_am }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Name</label>
                            <input type="text" name="name_ru" value="{{ $main_category->name_ru }}" class="form-control" required>
                        </div>
                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Edit
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>


    </div>




@endsection