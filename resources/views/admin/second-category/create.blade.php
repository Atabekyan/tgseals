{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 3/5/2019--}}
 {{--* Time: 11:53 AM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Categories</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Categories</a></li>
                        <li><a href="{{ route('second-category.getIndex') }}">Second categories</a></li>
                        <li class="active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Second Category</strong> create
                </div>
                <form id="add-second-category-form" method="POST" action="{{ route('second-category.postCreate') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group card-body card-block">
                        <div class="form-group">
                            <label class="form-control-label" >Select Main Category</label>
                            <select name="parent_id" class="form-control select-category" style="display: inline-block">
                                <option data-optional="false" value='' >Select</option>
                                @foreach($categories as $key => $value)
                                    <option data-optional="false" data-icon="true" value="{{ $value->id }}" required>&emsp;{{ $key + 1 }} : {{ $value->name_en }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input English Name</label>
                            <input type="text" name="name_en" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Name</label>
                            <input type="text" name="name_am" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Name</label>
                            <input type="text" name="name_ru" class="form-control" required>
                        </div>
                        <div class="form-group icon-upload">
                            <label class="btn btn-info btn-file">
                                Download Image <input type="file" id="upload-img" name="img" style="display: none" required>
                            </label>
                            <span class="img-name"></span>
                        </div>

                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>


    </div>




@endsection