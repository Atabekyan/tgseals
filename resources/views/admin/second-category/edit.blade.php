{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/5/2019--}}
{{--* Time: 11:54 AM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Categories</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Categories</a></li>
                        <li><a href="{{ route('second-category.getIndex') }}">Second categories</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Second Category</strong> edit
                </div>
                <form id="add-second-category-form" method="POST" action="{{ route('second-category.postUpdate') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $second_category->id }}">
                    <div class="form-group card-body card-block">
                        <div class="form-group">
                            <label class="form-control-label" >Select Main Category</label>
                            <select name="parent_id" class="form-control select-category" style="display: inline-block">
                                <option data-optional="false" value='' >Select</option>
                                @foreach( $main_category as $key => $value)
                                    <option data-optional="false" data-icon="true" value="{{ $value->id }}" {{ $value->id == $second_category->parent_id ? 'selected' : '' }}>
                                        {{ $key + 1 }} : {{ $value->name_en }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input English Name</label>
                            <input type="text" name="name_en" value="{{ $second_category->name_en }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Name</label>
                            <input type="text" name="name_am" value="{{ $second_category->name_am }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Name</label>
                            <input type="text" name="name_ru" value="{{ $second_category->name_ru }}" class="form-control" required>
                        </div>
                        @if(!is_null($second_category->img))
                            <div class="form-group img-upload">
                                <label class="btn btn-info btn-file">
                                    Download Image
                                    <input type="file" id="upload-type-img" name="img" style="display: none">
                                </label>
                                <span class="type-img-name">{{ $second_category->img }}</span>
                            </div>
                        @endif

                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection