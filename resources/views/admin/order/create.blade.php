{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 4/20/2019--}}
{{--* Time: 8:22 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Basket</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('basket.getIndex')}}">Basket</a></li>
                        <li class="active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Basket</strong> create
                </div>
                <form id="add-basket-form" method="POST" action="{{ route('basket.postCreate') }}">
                    {{ csrf_field() }}
                    <div class="form-group card-body card-block">
                        <div class="form-group">
                            <label class="form-control-label">Username</label>
                            <input type="text" name="user_id" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Product Group</label>
                            <input type="text" name="product_group" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Type</label>
                            <input type="text" name="type" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Compound</label>
                            <input type="text" name="compound" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Shore</label>
                            <input type="text" name="shore" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Special</label>
                            <input type="text" name="special" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Color Special</label>
                            <input type="text" name="color_special" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Dimension</label>
                            <input type="text" name="dimension" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Inner</label>
                            <input type="text" name="inner" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Outer</label>
                            <input type="text" name="outer" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Thickness</label>
                            <input type="text" name="thickness" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">High</label>
                            <input type="text" name="high" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Quantity</label>
                            <input type="text" name="quantity" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Is Available</label>
                            <input type="text" name="is_available" class="form-control">
                        </div>

                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection