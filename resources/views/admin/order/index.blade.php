{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/20/2019--}}
 {{--* Time: 8:22 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Orders</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Orders</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Orders</strong>
                    <a href="{{ route('order.getCreate') }}" class="add-btn btn btn-warning btn-sm">Add in Orders</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">User Name</th>
                            <th scope="col">Prod. group</th>
                            <th scope="col">Type</th>
                            <th scope="col">Compound</th>
                            <th scope="col">Shore</th>
                            <th scope="col">Special</th>
                            <th scope="col">Color</th>
                            <th scope="col">Dimension</th>
                            <th scope="col">Inner</th>
                            <th scope="col">Outer</th>
                            <th scope="col">Thickness</th>
                            <th scope="col">High</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Is available</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order as $key => $value)
                            <tr class="main-category" data-id="{{ $value->id }}">
                                <th scope="row">#{{ $key + 1 }}</th>
                                <td>{{ $value->user_id }}</td>
                                <td>{{ $value->product_group }}</td>
                                <td>{{ $value->type }}</td>
                                <td>{{ $value->compound }}</td>
                                <td>{{ $value->shore }}</td>
                                <td>{{ $value->special }}</td>
                                <td>{{ $value->color_specials }}</td>
                                <td>{{ $value->dimension }}</td>
                                <td>{{ $value->inner }}</td>
                                <td>{{ $value->outer }}</td>
                                <td>{{ $value->thickness }}</td>
                                <td>{{ $value->high }}</td>
                                <td>{{ $value->quantity }}</td>
                                <td>{{ $value->is_available }} </td>
                                <td>
                                    <a href="{{ route('order.getUpdate' , ['id' => $value->id]) }}"
                                       class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_modal_{{ $value->id }}">
                                        Delete
                                    </button>

                                    <div class="modal fade" id="delete_modal_{{ $value->id }}"  tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="smallmodalLabel">Delete product from order?!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        Are you sure, you want to delete form order of user???
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ route('order.getDelete' , ['id' => $value->id]) }}" class="btn btn-danger btn-sm">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
