
@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Store</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">E-Shop</a></li>
                        <li class="active">Store For Search</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Store For Search</strong><br>
                    <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" class="form-control" required><br>
                        <button class="btn btn-success">Import User Data</button>
                        <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Prod. group</th>
                            <th scope="col">Type</th>
                            <th scope="col">Compound</th>
                            <th scope="col">Shore</th>
                            <th scope="col">Special</th>
                            <th scope="col">Color</th>
                            <th scope="col">Dimension</th>
                            <th scope="col">Inner Min</th>
                            <th scope="col">Inner Max</th>
                            <th scope="col">Outer Min</th>
                            <th scope="col">Outer Max</th>
                            <th scope="col">Thickness Min</th>
                            <th scope="col">Thickness Max</th>
                            <th scope="col">High Min</th>
                            <th scope="col">High Max</th>
                            <th scope="col">High H Min</th>
                            <th scope="col">High H Max</th>
                            <th scope="col">Shaft Min</th>
                            <th scope="col">Shaft Max</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="main-category">
                                @foreach($store as $key => $value)
                                    <th scope="row">#{{ $key + 1 }}</th>
                                    <td>{{ $value->product_group }}</td>
                                    <td>{{ $value->type }}</td>
                                    <td>{{ $value->compound }}</td>
                                    <td>{{ $value->shore }}</td>
                                    <td>{{ $value->special }}</td>
                                    <td>{{ $value->color_specials }}</td>
                                    <td>{{ $value->dimension }}</td>
                                    <td>{{ $value->inner_min }}</td>
                                    <td>{{ $value->inner_max }}</td>
                                    <td>{{ $value->outer_min }}</td>
                                    <td>{{ $value->outer_max }}</td>
                                    <td>{{ $value->thickness_cross_section_min }}</td>
                                    <td>{{ $value->thickness_cross_section_max }}</td>
                                    <td>{{ $value->high_min }}</td>
                                    <td>{{ $value->high_max }}</td>
                                    <td>{{ $value->high_H_min }}</td>
                                    <td>{{ $value->high_H_max }}</td>
                                    <td>{{ $value->shaft_min }}</td>
                                    <td>{{ $value->shaft_min }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
