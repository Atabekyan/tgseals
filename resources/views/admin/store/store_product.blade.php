{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 7/17/2019--}}
 {{--* Time: 10:05 AM--}}
 {{--*/--}}


@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Store Products</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">E-Shop</a></li>
                        <li class="active">Store Products In Stock</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Store Products In Stock</strong><br>
                    <form action="{{ route('import.product') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" class="form-control" required><br>
                        <button class="btn btn-success">Import Product Data</button>
                        <a class="btn btn-warning" href="{{ route('export.product') }}">Export Product Data</a>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Prod. group</th>
                            <th scope="col">Type</th>
                            <th scope="col">Compound</th>
                            <th scope="col">Shore</th>
                            <th scope="col">Special</th>
                            <th scope="col">Color</th>
                            <th scope="col">Dimension</th>
                            <th scope="col">Inner</th>
                            <th scope="col">Outer</th>
                            <th scope="col">Thickness</th>
                            <th scope="col">High</th>
                            <th scope="col">High H</th>
                            <th scope="col">Shaft</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Is available</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="main-category">
                            @foreach($products as $key => $value)
                                <th scope="row">#{{ $value->id }}</th>
                                <td>{{ $value->product_group }}</td>
                                <td>{{ $value->type }}</td>
                                <td>{{ $value->compound }}</td>
                                <td>{{ $value->shore }}</td>
                                <td>{{ $value->special }}</td>
                                <td>{{ $value->color_specials }}</td>
                                <td>{{ $value->dimension }}</td>
                                <td>{{ $value->inner }}</td>
                                <td>{{ $value->outer }}</td>
                                <td>{{ $value->thickness }}</td>
                                <td>{{ $value->high }}</td>
                                <td>{{ $value->high_H }}</td>
                                <td>{{ $value->shaft }}</td>
                                <td>{{ $value->price }}</td>
                                <td>{{ $value->quantity }}</td>
                                <td>{{ $value->is_available }} </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>


@endsection
