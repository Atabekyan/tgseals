{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 5/24/2019--}}
 {{--* Time: 3:56 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li><a href="{{ route('e-shop.getIndex') }}">E-Shop</a></li>
                        <li class="active">Create IMG</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>E-Shop</strong> create IMG
                </div>
                <form id="add-e-shop-form" method="POST" action="{{ route('e-shop-img.postCreate') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="page_name" value="e_shop">
                    <div class="form-group card-block">

                        <div class="form-group img-upload">
                            <label class="btn btn-info btn-file">
                                Download Image <input type="file" id="upload-img" name="url" style="display: none" required>
                            </label>
                            <span class="img-name"></span>
                        </div>

                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection