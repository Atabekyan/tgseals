{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/5/2019--}}
 {{--* Time: 8:07 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li class="active">Main</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Main</strong>
                    <a href="{{ route('main-content.getCreate') }}" class="add-btn btn btn-warning btn-sm content-btn">Add Content</a>
                    {{--<a href="{{ route('main-header.getCreate') }}" class="add-btn btn btn-warning btn-sm header-btn">Add Header</a>--}}
                </div>

                <div class="card-body">
                    @foreach($main as $key => $value)
                        @if (!$value->header_en == null)
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Header EN</th>
                            <th scope="col">Header AM</th>
                            <th scope="col">Header RU</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="main-category" data-id="{{ $value->id }}">
                                <th scope="row">#{{ $key + 1 }}</th>
                                <td>{{ $value->header_en }}</td>
                                <td>{{ $value->header_am }}</td>
                                <td>{{ $value->header_ru }}</td>
                                <td>
                                    <a href="{{ route('main-header.getUpdate' , ['id' => $value->id]) }}"
                                       class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_modal_{{ $value->id }}">
                                        Delete
                                    </button>

                                    <div class="modal fade" id="delete_modal_{{ $value->id }}"  tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="smallmodalLabel">Delete header?!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        Are you sure, you want to delete this header???
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ route('main.getDelete' , ['id' => $value->id]) }}" class="btn btn-danger btn-sm">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                    @if (!$value->content_en == null)
                        <table class="table">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Content EN</th>
                                <th scope="col">Content AM</th>
                                <th scope="col">Content RU</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="main-category" data-id="{{ $value->id }}">
                                <th scope="row">#{{ $key + 1 }}</th>
                                <td>{!! explode('!N!' , $value->content_en)[0] !!}</td>
                                <td>{!! explode('!N!' , $value->content_am)[0] !!}</td>
                                <td>{!! explode('!N!' , $value->content_ru)[0] !!}</td>
                                <td>
                                    <a href="{{ route('main-content.getUpdate' , ['id' => $value->id]) }}"
                                       class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_modal_{{ $value->id }}">
                                        Delete
                                    </button>

                                    <div class="modal fade" id="delete_modal_{{ $value->id }}"  tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="smallmodalLabel">Delete content?!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        Are you sure, you want to delete this content???
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ route('main.getDelete' , ['id' => $value->id]) }}" class="btn btn-danger btn-sm">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>


@endsection
