{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/5/2019--}}
 {{--* Time: 8:07 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li><a href="{{ route('main-admin.getIndex') }}">Main</a></li>
                        <li class="active">Edit Header</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Main</strong> edit header
                </div>
                <form id="edit-about-us-form" method="POST" action="{{ route('main-header.postUpdate') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $main->id }}">
                    <input type="hidden" name="page_name" value="main">
                    <div class="form-group card-block">
                        <div class="form-group">
                            <label class="form-control-label">Input English Header</label>
                            <input type="text" name="header_en" value="{{ $main->header_en }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Header</label>
                            <input type="text" name="header_am" value="{{ $main->header_am }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Header</label>
                            <input type="text" name="header_ru" value="{{ $main->header_ru }}" class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection