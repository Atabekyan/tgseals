{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/6/2019--}}
 {{--* Time: 6:16 PM--}}
 {{--*/--}}

{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 4/5/2019--}}
{{--* Time: 8:07 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li><a href="{{ route('main-admin.getIndex') }}">Main</a></li>
                        <li class="active">Edit Content</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Main</strong> edit content
                </div>
                <form id="edit-about-us-content-form" method="POST" action="{{ route('main-content.postUpdate') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $main->id }}">
                    <input type="hidden" name="page_name" value="main">
                    <div class="form-group card-body card-block">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input English Content</label>
                                <textarea class="form-control" id="editor_en" name="content_en" value="{{ $main->content_en }}" required></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input Armenian Content</label>
                                <textarea class="form-control" id="editor_am" name="content_am" value="{{ $main->content_am }}" required></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input Russian Content</label>
                                <textarea class="form-control" id="editor_ru" name="content_ru" value="{{ $main->content_ru }}" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@section('script')
    {{--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/admin/pages/script.js"></script>
@endsection




@endsection