{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/5/2019--}}
 {{--* Time: 8:07 PM--}}
 {{--*/--}}


@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li><a href="{{ route('applications.getIndex') }}">Applications</a></li>
                        <li class="active">Create Header</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Applications</strong> create header
                </div>
                <form id="add-main-category-form" method="POST" action="{{ route('applications-header.postCreate') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="page_name" value="applications">
                    <div class="form-group card-block">
                        <div class="form-group">
                            <label class="form-control-label">Input English Header</label>
                            <input type="text" name="header_en" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Armenian Header</label>
                            <input type="text" name="header_am" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Input Russian Header</label>
                            <input type="text" name="header_ru" class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection