{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 5/24/2019--}}
 {{--* Time: 3:56 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Pages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Pages</a></li>
                        <li><a href="{{ route('applications.getIndex') }}">Applications</a></li>
                        <li class="active">Create IMG</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <strong>Applications</strong> create IMG
                </div>
                <form id="add-main-category-form" method="POST" action="{{ route('applications-img.postCreate') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="page_name" value="applications">
                    <div class="form-group card-block">

                        <div class="form-group img-upload">
                            <label class="btn btn-info btn-file">
                                Download Image <input type="file" id="upload-img" name="url" style="display: none" required>
                            </label>
                            <span class="img-name"></span>
                        </div>

                    </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection