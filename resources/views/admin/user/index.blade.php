{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/20/2019--}}
 {{--* Time: 8:22 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Users</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li class="active">Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Users</strong>
                    <a href="{{ route('user.getCreate') }}" class="add-btn btn btn-warning btn-sm">Add user</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">User first name</th>
                            <th scope="col">User surname</th>
                            <th scope="col">Company Name</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Vat</th>
                            <th scope="col">Address</th>
                            <th scope="col">User Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Registered date </th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $key => $value)
                            <tr class="main-category" data-id="{{ $value->id }}">
                                <th scope="row">#{{ $key + 1 }}</th>
                                <td>{{ $value->first_name }}</td>
                                <td>{{ $value->surname }}</td>
                                <td>{{ $value->company_name }}</td>
                                <td>{{ $value->phone }}</td>
                                <td>{{ $value->address }}</td>
                                <td>{{ $value->email }}</td>
                                @if($value->role == 'guest')
                                    <td style="color: red">{{ $value->role }}</td>
                                @elseif($value->role == 'admin')
                                    <td style="color: blue">{{ $value->role }}</td>
                                @elseif($value->role == 'user')
                                    <td style="color: green">{{ $value->role }}</td>
                                @endif
                                <td>{{ $value->created_at }}</td>
                                <td>
                                    <a href="{{ route('user.getUpdate' , ['id' => $value->id]) }}"
                                       class="btn btn-primary btn-sm">Edit</a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_modal_{{ $value->id }}">
                                        Delete
                                    </button>

                                    <div class="modal fade" id="delete_modal_{{ $value->id }}"  tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="smallmodalLabel">Delete user?!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        Are you sure, you want to delete <b>{{ $value->name }}</b> form users???
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ route('user.getDelete' , ['id' => $value->id]) }}" class="btn btn-danger btn-sm">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
