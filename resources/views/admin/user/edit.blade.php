{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 4/20/2019--}}
{{--* Time: 8:22 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>User</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('user.getIndex')}}">User</a></li>
                        <li class="active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>User</strong> create
                </div>
                <form id="add-user-form" method="POST" action="{{ route('user.postUpdate') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="form-group card-body card-block">
                            <div class="form-group">
                                <label class="form-control-label">User Name</label>
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                       value="{{$user->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group card-body card-block">
                            <div class="form-group">
                                <label class="form-control-label">Company Name</label>
                                <input id="company_name" type="text"
                                       class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}"
                                       name="company_name"
                                       value="{{$user->company_name}}" required autofocus>

                                @if ($errors->has('company_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group card-body card-block">
                            <div class="form-group">
                                <label class="form-control-label">Phone</label>
                                <input id="phone" type="text"
                                       class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone"
                                       value="{{$user->phone}}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">User Email</label>
                            <input id="email" type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ $user->email}}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Password: If you don't want to change the password <b>don't
                                    fill this</b> and <b>next</b> fields</label>
                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Confirm password</label>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">User role</label>
                            <select id="select_role" class="form-control" name="role">
                                <option style="color: blue" value="admin" {{ $user->role == 'admin' ? 'selected' : ''  }}>Admin</option>
                                <option style="color: green" value="user" {{ $user->role == 'user' ? 'selected' : ''  }}>User</option>
                                <option style="color: red" value="guest" {{ $user->role == 'guest' ? 'selected' : ''  }}>Guest</option>
                            </select>
                        </div>
                    <div class="card-footer">

                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection