{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 6/6/2019--}}
 {{--* Time: 2:06 PM--}}
 {{--*/--}}

{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 28-Feb-19--}}
{{--* Time: 6:00 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Messages</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Messages</a></li>
                        {{--<li class="active">Main categories</li>--}}
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Main Categories</strong>
                    <a href="{{ route('main-category.getCreate') }}" class="add-btn btn btn-warning btn-sm">Add category</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Topic</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email & Phone</th>
                            <th scope="col">Message</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $key => $value)
                            <tr class="main-category" data-id="{{ $value->id }}">
                                <th scope="row">#{{ $key + 1 }}</th>
                                <td>{{ $value->topic }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->email }} <br> {{ $value->phone }}</td>
                                <td>{{ $value->message }}</td>
                                <td>
                                    {{--<a href="{{ route('main-category.getUpdate' , ['id' => $value->id]) }}"--}}
                                       {{--class="btn btn-primary btn-sm">Edit</a>--}}
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete_modal_{{ $value->id }}">
                                        Delete
                                    </button>

                                    <div class="modal fade" id="delete_modal_{{ $value->id }}"  tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="smallmodalLabel">Delete category?!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        Are you sure, you want to delete <b>{{ $value->name_en }}</b> category???
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button>
                                                    <a href="{{ route('messages.getDelete' , ['id' => $value->id]) }}" class="btn btn-danger btn-sm">Confirm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
