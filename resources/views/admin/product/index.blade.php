{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/19/2019--}}
{{--* Time: 5:41 PM--}}
{{--*/--}}

@extends('layouts.admin')

@section('assets')
    <link rel="apple-touch-icon" href="/admin_assets/apple-icon.png">
    <link rel="shortcut icon" href="/admin_assets/favicon.ico">
    <link rel="stylesheet" href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/admin_assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/admin_assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="/admin_assets/assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Products</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        {{--<li><a href="#">Product</a></li>--}}
                        <li class="active">Product</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Product list</strong>
                            <a href="{{ route('product.getCreate') }}" class="add-btn btn btn-warning btn-sm">Add
                                product</a>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Type IMG</th>
                                    <th>Type</th>
                                    <th>Product</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $value)
                                        <tr>
                                            <th scope="row">#{{ $value->id }}</th>
                                            <td>
                                                @if($value->type_img != null )
                                                    <img src="/images/product/type/{{ $value->type_img }}" width="30"
                                                         height="30">
                                                @endif</td>
                                            <td>{{ $value->type }}</td>
                                            <td>{{ $value->name_en }}</td>
                                            <td>{!! explode('!N!' , $value->getAttribute('description_en'))[0] !!}</td>
                                            <td>
                                                <a href="{{ route('product.getUpdate' , ['id' => $value->id]) }}"
                                                   class="btn btn-primary btn-sm">Edit</a>
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                                        data-target="#delete_modal_{{ $value->id }}">
                                                    Delete
                                                </button>
                                                <a href="{{ route('product.getCreateSettings' , ['id' => $value->id]) }}"
                                                   class="btn btn-secondary btn-sm">Add Settings</a>

                                                <div class="modal fade" id="delete_modal_{{ $value->id }}" tabindex="-1"
                                                     role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="smallmodalLabel">Delete
                                                                    product?!</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Are you sure, you want to delete
                                                                    <b>{{ $value->name_en }}</b> product with it's
                                                                    settingsg???
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary btn-sm"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <a href="{{ route('product.getDelete' , ['id' => $value->id]) }}"
                                                                   class="btn btn-danger btn-sm">Confirm</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @foreach($value->product_settings as $key_settings => $value_settings)
                                        <tr>
                                            <th>-</th>
                                            <td>{{ $key_settings + 1 }}</td>
                                            <td>{{ $value_settings->material_en }}</td>
                                            <td>{{ $value_settings->pressure }}</td>
                                            <td>{{ $value_settings->dimensions }}
                                            <td>
                                                <a class="btn btn-primary btn-sm"
                                                   href="{{ route('product.getUpdateSettings' , ['id' => $value_settings->id]) }}">Edit</a>

                                                <a class="btn btn-danger btn-sm"
                                                   href="{{ route('product.getDeleteSettings' , ['id' => $value_settings->id]) }}">Delete</a>

                                            </td>
                                        </tr>
                                        @endforeach
                                @endforeach
                                </tbody>
                            </table>
                            {{ $products->onEachSide(1)->links() }}
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


@section('script')

    <script src="/admin_assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin_assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="/admin_assets/assets/js/init-scripts/data-table/datatables-init.js"></script>
@endsection


@endsection
