{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/19/2019--}}
{{--* Time: 5:42 PM--}}
{{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Products</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('product.getIndex')}}">Product</a></li>
                        <li class="active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Product</strong> create
                </div>
                <form id="add-product-form" method="POST" action="{{ route('product.postCreate') }}" enctype="multipart/form-data">

                    {{ csrf_field() }}
                    <div class="form-group card-body card-block">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-control-label">Input English Name</label>
                                    <input type="text" name="name_en" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Input Armenian Name</label>
                                    <input type="text" name="name_am" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Input Russian Name</label>
                                    <input type="text" name="name_ru" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Select Category</label>
                                    <select id="add-select-category" class="form-control select-category" name="second_category_id">
                                        <option value=''>Select Second Category</option>
                                        @foreach($main_category as $key => $value)
                                            <optgroup value="{{ $value->id }}" style="background-color: #ffa31a" label="{{ $key + 1 }} : {{ $value->name_en }}">
                                                @foreach($value->subcategories as $key => $value)
                                                    <option data-optional="false" data-icon="true" value="{{ $value->id }}" style="background-color: #ffd699">&emsp;&emsp;{{ $key + 1 }} : {{ $value->name_en }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Input Type</label>
                                    <input type="text" name="type" class="form-control">
                                </div>
                                <div class="form-group img-upload">
                                    <label class="btn btn-info btn-file">
                                        Download Type Image
                                        <input type="file" id="upload-img" name="type_img" style="display: none" required>
                                    </label>
                                    <span class="img-name"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input English Description</label>
                                <textarea class="form-control" id="editor_en" name="description_en"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input Armenian Description</label>
                                <textarea class="form-control" id="editor_am" name="description_am"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Input Russian Description</label>
                                <textarea class="form-control" id="editor_ru" name="description_ru"></textarea>
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label class="btn btn-info btn-file">--}}
                                {{--Загрузить картинки <input type="file" id="upload-product-images" multiple style="display: none">--}}
                                {{--<input type="hidden" id="uploaded-product-images" name="images" >--}}
                            {{--</label>--}}
                            {{--<div class="product-images">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>


    </div>

@section('script')
    {{--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/admin/product/create.js"></script>
@endsection


@endsection