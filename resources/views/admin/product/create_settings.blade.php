{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 5/6/2019--}}
 {{--* Time: 6:34 PM--}}
 {{--*/--}}

@extends('layouts.admin')
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Products</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="{{ route('product.getIndex')}}">Product</a></li>
                        <li class="active">Create Settings</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Create settings for <strong>{{ $product->name_en }}</strong> product.
                </div>
                <form id="add-product-form" method="POST" action="{{ route('product.postCreateSettings') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <div class="form-group card-body card-block">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-control-label">Input English Material Name</label>
                                    <input type="text" name="material_en" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Input Armenian Material Name</label>
                                    <input type="text" name="material_am" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Input Russian Material Name</label>
                                    <input type="text" name="material_ru" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Speed</label>
                                    <input type="text" name="speed" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="form-control-label">Min Temperature</label>
                                    <input type="text" name="temp_min" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Max Temperature</label>
                                    <input type="text" name="temp_max" class="form-control">
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Pressure</label>
                                    <input type="text" name="pressure" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Dimensions</label>
                                    <input type="text" name="dimensions" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Input Quantity</label>
                                        <input type="number" name="quantity" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Is Available</label>
                                        <br>
                                        <label class="switch switch-3d switch-success mr-3">
                                            <input type="checkbox" name="is_available" class="switch-input" checked="true">
                                            <span class="switch-label"></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Add
                        </button>
                        <a href="{{ URL::previous() }}" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cansel
                        </a>
                    </div>
                </form>
            </div>
        </div>


    </div>

@section('script')
    {{--<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/admin/product/create.js"></script>
@endsection


@endsection