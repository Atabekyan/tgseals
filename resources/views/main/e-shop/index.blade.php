<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TGSeals</title>
    <link rel="icon" href="/images/sidebar_logo.png"/>

    <link href="/main_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/main_assets/css/shop-homepage.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    @yield('css')

    <link rel="stylesheet" href="/css/main/style.css"/>
    <link rel="stylesheet" href="/css/main/layout.css"/>
    <link rel="stylesheet" href="/css/main/media.css"/>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        var BASE_URL = '<?php echo e(env('APP_URL')); ?>'
    </script>

</head>
<body>

    <div id="app">

    </div>

        <script src="/js/app.js"></script>
</body>
</html>