{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 3/25/2019--}}
 {{--* Time: 11:44 PM--}}
 {{--*/--}}

@extends('layouts.main-first')

@section('content')

    <div class="main-content-full">
            <div class="main-logo">
                <img src="../../images/main_logo.png">
            </div>
            <div class="col-lg-12 main-pages">
                <a class="col-lg-2 col-sm-8" href="{{ route('main.getAboutUs') }}">{{  __('messages.about_us') }}</a>
                <a class="col-lg-2 col-sm-8" href="{{ route('products.getProducts') }}">{{  __('messages.products') }}</a>
                <a class="col-lg-2 col-sm-8" href="{{ route('main.getApplications') }}" >{{  __('messages.applications') }}</a>
                <a class="col-lg-2 col-sm-8" href="{{ route('main.getMaterials') }}">{{  __('messages.materials') }}</a>
                <a class="col-lg-2 col-sm-8" href="{{ route('main.getContacts') }}">{{  __('messages.contact') }}</a>
                <a class="col-lg-2 col-sm-8" href="@if (\Auth::check()) {{route('main.getEShopIndex')}} @else {{route('main.getEShop')}} @endif " >{{  __('messages.e_shop') }}</a>

            </div>

            @foreach($main as $key => $value)
            <div class="main-page-text">
                {!! explode('!N!' , $value->getAttribute('content_'.App::getLocale()))[0] !!}
            </div>
            @endforeach

    </div>


@endsection