{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/2/2019--}}
 {{--* Time: 8:00 PM--}}
 {{--*/--}}

@extends('layouts.main')

@section('content')

    <div class="col-lg-9">
        <div class="list-group">
            <div class="imprint-page-title">
                <h2>
                    {{  __('messages.imprint') }}
                </h2>
            </div>
        </div>

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($imprint_img as $key)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$key}}" class="active"></li>
                @endforeach
            </ol>

            <div class="carousel-inner" role="listbox">
                @foreach($imprint_img as $key => $img)
                    <div class="carousel-item {{ $key == 0 ? 'active' : ''  }}">
                        <img class="d-block img-fluid" src="/images/pages/{{ $img->url }}" alt="Slide" style="width: 900px; height: 350px">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="page-content">
            @foreach($imprint as $key => $value)
                @if (!$value->header_en == null)
                    <div class="col-lg-12 col-md-6 mb-8">
                        <div class="page-title">
                            <h3>
                                {{ $value->getAttribute('header_'.App::getLocale()) }}
                            </h3>
                        </div>
                    </div>
                @endif
                @if (!$value->content_en == null)
                    <div class="col-lg-12 col-md-6 mb-8">
                        <div class="content-text">
                            <p>
                                {!! explode('!N!' , $value->getAttribute('content_'.App::getLocale()))[0] !!}
                            </p>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>


    </div>
    <!-- /.col-lg-9 -->








@endsection