{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 4/2/2019--}}
{{--* Time: 3:56 PM--}}
{{--*/--}}

@extends('layouts.main')

@section('content')

    <div class="col-lg-9 col-md-12 mb-12 ">

        <div class="list-group">
            <div class="contacts-page-title">
                <h2>
                    {{  __('messages.contact') }}
                </h2>
            </div>
        </div>
        <div class="contacts-content">
            @foreach($contact as $key => $value)
                @if (!$value->header_en == null)
                    <div class="col-lg-12 col-md-6 mb-8">
                        <div class="page-title">
                            <h3>
                                {{ $value->getAttribute('header_'.App::getLocale()) }}
                            </h3>
                        </div>
                    </div>
                @endif
                @if (!$value->content_en == null)
                    <div class="col-lg-6 col-md-6 mb-12">
                        <div class="contacts-address">
                            <p>
                                {!! explode('!N!' , $value->getAttribute('content_'.App::getLocale()))[0] !!}
                            </p>
                        </div>
                    </div>
                @endif
            @endforeach

            <div class="col-lg-12">
                <form method="POST" action="{{ route('main.postContacts') }}">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header contact-page-middle-title">
                            <strong>{{  __('messages.make_contact_with_us') }}</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row form-group">
                                <div class="col col-md-6"><label for="select" class=" form-control-label">{{  __('messages.select_topic_in_menu') }}</label>
                                </div>
                                <div class="col-12 col-md-6">
                                    <select name="topic" id="select" class="form-control">
                                        <option value="Sales">{{  __('messages.sales') }}</option>
                                        <option value="Technical question">{{  __('messages.technical_question') }}</option>
                                        <option value="Quality">{{  __('messages.quality') }}</option>
                                        <option value="Logistic">{{  __('messages.logistic') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">{{  __('messages.first_name') }}</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="text-input" name="name" placeholder="" class="form-control" required>
                                </div>
                            </div>
                            {{--<div class="row form-group">--}}
                            {{--<div class="col col-md-3"><label for="text-input" class=" form-control-label">{{  __('messages.last_name') }}</label></div>--}}
                            {{--<div class="col-12 col-md-9"><input type="text" id="text-input" name="text-input" placeholder="" class="form-control"></div>--}}
                            {{--</div>--}}
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">{{  __('messages.company_name') }}</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="text-input" name="company_name" placeholder="" class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="email-input" class=" form-control-label">{{  __('messages.email') }}</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="email" id="email-input" name="email" placeholder="{{  __('messages.enter_email') }}" class="form-control" required>
                                    <small class="help-block form-text">{{  __('messages.please_enter_email') }}</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="phone-input" class=" form-control-label">{{  __('messages.phone') }}</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="phone-input" name="phone" placeholder="{{  __('messages.enter_phone_number') }}" class="form-control" required>
                                    <small class="help-block form-text">{{  __('messages.enter_phone_number_with_code') }}</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="textarea-input" class=" form-control-label">{{  __('messages.message') }}</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <textarea name="message" id="textarea-input" rows="9" placeholder="{{  __('messages.content') }}" class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class="form-control-label">{{  __('messages.make_contact_with_you') }}</label>
                                </div>
                                <div class="col col-md-9">
                                    <div class=" form-check">
                                        <label for="inline-radio1" class="form-check-label ">
                                            <input type="radio" id="inline-radio1" name="contact_form" value="Email"
                                                   class="form-check-input">{{  __('messages.email') }}
                                        </label>
                                        <label for="inline-radio2" class="form-check-label ">
                                            <input type="radio" id="inline-radio2" name="contact_form" value="Phone"
                                                   class="form-check-input">{{  __('messages.phone') }}
                                        </label>
                                        <label for="inline-radio3" class="form-check-label ">
                                            <input type="radio" id="inline-radio3" name="contact_form" value="Email & Phone"
                                                   class="form-check-input" checked>{{  __('messages.both') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> {{  __('messages.send') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
    <!-- /.col-lg-9 -->








@endsection