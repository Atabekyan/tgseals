{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 4/12/2019--}}
{{--* Time: 4:54 PM--}}
{{--*/--}}

<nav class="navbar navbar-expand-lg navbar-dark bg-dark header fixed-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main.getIndex') }}">{{  __('messages.home') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main.getAboutUs') }}">{{  __('messages.about_us') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('products.getProducts') }}" >{{  __('messages.products') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main.getMaterials') }}">{{  __('messages.materials') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main.getApplications') }}" >{{  __('messages.applications') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('main.getEShop') }}" >{{  __('messages.e_shop') }}</a>
                </li>
            </ul>
        </div>
        <div class="header-to-right">
            <ul class="header-items">
                <li class="nav-item">
                    <div class="dropdown language-bar">
                        <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
                            <img class="lang-icon"
                                 src="{{ Session::get('locale') ? '/images/'.Session::get('locale').'.png' : '/images/en.png' }}">
                            <i class="fas fa-chevron-down lang-arrow"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li {{Session::get('locale') == null || Session::get('locale') == 'en' ? 'style=display:none' : ''}}>
                                <a data-lang="en" class="select-lang"><img src="/images/en.png" alt=""/></a>
                            </li>
                            <li {{Session::get('locale') == 'ru' ? 'style=display:none' : ''}}>
                                <a data-lang="ru" class="select-lang"><img src="/images/ru.png" alt=""/></a>
                            </li>
                            <li {{Session::get('locale') == 'am' ? 'style=display:none' : ''}}>
                                <a data-lang="am" class="select-lang"><img src="/images/am.png" alt=""/></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item contact-button">
                    <a class="contact-button-link" href="{{ route('main.getContacts') }}">{{__('messages.contact') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>