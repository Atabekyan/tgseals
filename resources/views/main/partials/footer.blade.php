{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 3/26/2019--}}
 {{--* Time: 3:56 PM--}}
 {{--*/--}}

<footer class="footer-body">
    <div class="footer-content">
        <div class="footer-links " >

            <div class="footer-links-top">
                <div style="width: 30%" class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('main.getEShop') }}" >{{  __('messages.e_shop') }}</a></a>
                </div>
                <div style="width: 30%" class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('main.getAboutUs') }}">{{  __('messages.about_us') }}</a>
                </div>
                <div style="width: 40%" class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('main.getApplications') }}" >{{  __('messages.applications') }}</a>
                </div>
            </div>

            <div class="footer-links-bottom">
                <div style="width: 30%" class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('products.getProducts') }}">{{  __('messages.products') }}</a>
                </div>
                <div  style="width: 30%"class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('main.getContacts') }}">{{  __('messages.contact') }}</a>
                </div>
                <div style="width: 40%" class="footer-link-style">
                    <img src="/images/icons/footer_icon.png">
                    <a href="{{ route('main.getPrivacyPolicy') }}">{{  __('messages.privacy_policy_short') }}</a>
                </div>
            </div>

        </div>
        <div class="footer-logo">
            <img src="/images/main_logo.png">
        </div>

    </div>


    <div class="web-developer">
        <p class="m-0 text-center text-white">&copy; Copyright 2019</p>
        <p class="m-0 text-center text-white">All rights reserved. Powered by <b><a target="_blank" href="#">Delta Software </a></b></p>
    </div>
    <!-- /.container -->
</footer>