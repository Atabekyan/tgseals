{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 3/26/2019--}}
 {{--* Time: 3:55 PM--}}
 {{--*/--}}

<div class="col-lg-3 sidebar">
    <div class="scene">
        <div class="cube">
            <div class="cube__face cube__face--front">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
            <div class="hide_img cube__face cube__face--back">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
            <div class="hide_img cube__face cube__face--right">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
            <div class="hide_img cube__face cube__face--left">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
            <div class="hide_img cube__face cube__face--top">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
            <div class="hide_img cube__face cube__face--bottom">
                <a href="{{ route('main.getIndex') }}">
                    <img class="sidebar-icon-img" src="/images/sidebar_logo.png">
                </a>
            </div>
        </div>
    </div>
    <p>
        <label style="display: none">
            perspective
            <input class="perspective-range" type="range" min="1" max="1000" value="none" data-units="px" />
        </label>
    </p>
    <p>
        <label style="display: none">
            perspective-origin x
            <input class="origin-x-range" type="range" min="0" max="100" value="14" data-units="%" />
        </label>
    </p>
    <p>
        <label style="display: none">
            perspective-origin y
            <input class="origin-y-range" type="range" min="0" max="100" value="50" data-units="%" />
        </label>
    </p>
    <p>
        <label style="display: none">
            Spin cube
            <input class="spin-cube-checkbox" type="checkbox" checked/>
        </label>
    </p>
    <p>
        <label style="display: none">
            Backface visible
            <input class="backface-checkbox" type="checkbox" checked />
        </label>
    </p>


    {{--<div class="sidebar-icon">--}}
        {{--<a href="{{ route('main.getIndex') }}">--}}
            {{--<img class="sidebar-icon-img" src="/images/sidebar_logo.png">--}}
        {{--</a>--}}
    {{--</div>--}}

    <div class="list-group sidebar-link-group">
        <div class="list-group-item sidebar-links">
            <img src="/images/icons/tg_small_icon.png">
            <a href="{{ route('main.getAboutUs') }}">{{  __('messages.about_us') }}</a>
        </div>
        <div class="list-group-item sidebar-links">
            <img src="/images/icons/tg_small_icon.png">
            <a href="{{ route('products.getProducts') }}" >{{  __('messages.products') }}</a>
        </div>
        <div class="list-group-item sidebar-links">
            <img src="/images/icons/tg_small_icon.png">
            <a href="{{ route('main.getMaterials') }}">{{  __('messages.materials') }}</a>
        </div>
        <div class="list-group-item sidebar-links">
            <img src="/images/icons/tg_small_icon.png">
            <a href="{{ route('main.getApplications') }}" >{{  __('messages.applications') }}</a>
        </div>
        <div class="list-group-item sidebar-links">
            <img src="/images/icons/tg_small_icon.png">
            <a href="@if (\Auth::check()) {{route('main.getEShopIndex')}} @else {{route('main.getEShop')}} @endif " >{{  __('messages.e_shop') }}</a>
        </div>
        {{--<div class="list-group-item sidebar-links">--}}
            {{--<img src="/images/icons/tg_small_icon.png">--}}
            {{--<a href="{{ route('main.getCompounds') }}" >{{  __('messages.compounds') }}</a>--}}
        {{--</div>--}}

    </div>

</div>