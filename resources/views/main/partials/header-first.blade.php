{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/26/2019--}}
{{--* Time: 3:55 PM--}}
{{--*/--}}

<nav class="navbar navbar-expand-lg header fixed-top">
    <div class="container">
        <div class="header-to-right">
            <ul class="navbar-nav ml-auto header-items">
                <li class="nav-item">
                    <div class="dropdown language-bar">
                        <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">
                            <img class="lang-icon"
                                 src="{{ Session::get('locale') ? '/images/'.Session::get('locale').'.png' : '/images/en.png' }}">
                            <i class="fas fa-chevron-down lang-arrow"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li {{Session::get('locale') == null || Session::get('locale') == 'en' ? 'style=display:none' : ''}}>
                                <a data-lang="en" class="select-lang"><img src="/images/en.png" alt=""/></a>
                            </li>
                            <li {{Session::get('locale') == 'ru' ? 'style=display:none' : ''}}>
                                <a data-lang="ru" class="select-lang"><img src="/images/ru.png" alt=""/></a>
                            </li>
                            <li {{Session::get('locale') == 'am' ? 'style=display:none' : ''}}>
                                <a data-lang="am" class="select-lang"><img src="/images/am.png" alt=""/></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item contact-button">
                    <a class="contact-button-link" href="{{ route('main.getContacts') }}">{{__('messages.contact') }}</a>
                </li>
            </ul>
        </div>
    </div>
</nav>