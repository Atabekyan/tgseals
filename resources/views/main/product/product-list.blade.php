{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/28/2019--}}
{{--* Time: 11:22 PM--}}
{{--*/--}}

@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="/css/main/products/style.css"/>

    <link rel="stylesheet" href="/admin_assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="/admin_assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="/admin_assets/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin_assets/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    {{--<link rel="stylesheet" href="/admin_assets/assets/css/style.css">--}}
@endsection

@section('content')

    <div class="content col-lg-9">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card prod-table-border">
                        <div class="card-body search-title">
                            <table id="bootstrap-data-table-export"
                                   class="table table-striped table-bordered product-table">
                                <h4>{{  __('messages.products_group') }}</h4>
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Product</th>
                                    <th>Type</th>
                                    <th>Description</th>
                                    <th>Materials</th>
                                    <th colspan="2">Temperature [°C]<br>min.– max.</th>
                                    <th>Speed [m/s]</th>
                                    <th>Pressure <br> [MPA] max.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $key => $value)
                                    @php $productSettings = $value->product_settings @endphp

                                    @if (count($productSettings))
                                        @foreach($productSettings as $setting_key => $setting)
                                            <tr class="product" data-id="{{ $value->id }}">
                                                <td>
                                                    @if($value->type_img != null )
                                                        <img src="/images/product/type/{{ $value->type_img }}" width="30"
                                                             height="30">
                                                    @endif</td>
                                                </td>
                                                <td>{{ $value->getAttribute('name_'.App::getLocale()) }}</td>
                                                <td>{{ $value->type }}</td>
                                                <td>{!! explode('!N!' , $value->getAttribute('description_'.App::getLocale()))[0] !!}</td>
                                                <td>{{ is_null($setting->getAttribute('material_'.App::getLocale())) ? ' - ' : $setting->getAttribute('material_'.App::getLocale()) }}</td>
                                                <td>{{ is_null($setting->temp_min) ? ' - ' : $setting->temp_min  }}</td>
                                                <td>{{ is_null($setting->temp_max) ? ' - ' : $setting->temp_max}}</td>
                                                <td>{{ is_null($setting->speed) ? ' - ' : $setting->speed }}</td>
                                                <td>{{ is_null($setting->pressure) ? ' - ' : $setting->pressure }}</td>
                                                <td style="text-align: center"><a class="prod-eshop-link" href="{{ route('main.getEShop') }}"><i class="fas fa-shopping-cart"></i></a></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="product" data-id="{{ $value->id }}">
                                            <td>
                                                @if($value->type_img != null )
                                                    <img src="/images/product/type/{{ $value->type_img }}" width="30"
                                                         height="30">
                                                @endif</td>
                                            </td>
                                            <td>{{ $value->getAttribute('name_'.App::getLocale()) }}</td>
                                            <td>{{ $value->type }}</td>
                                            <td>{!! explode('!N!' , $value->getAttribute('description_'.App::getLocale()))[0] !!}</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td style="text-align: center"><a class="prod-eshop-link" href="{{ route('main.getEShop') }}"><i class="fas fa-shopping-cart"></i></a></td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@section('js')
    <script src="/admin_assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin_assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/admin_assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/admin_assets/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="/admin_assets/assets/js/init-scripts/data-table/datatables-init.js"></script>

@endsection


@endsection