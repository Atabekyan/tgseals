{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 3/27/2019--}}
 {{--* Time: 1:01 PM--}}
 {{--*/--}}
@extends('layouts.main')

@section('css')
    <link rel="stylesheet" href="/css/main/products/style.css"/>
@endsection

@section('content')

    <div class="col-lg-9">
        <div class="list-group">
            <div class="product-page-title">
                <h2>
                    {{  __('messages.products') }}
                </h2>
            </div>
            @foreach($main_category as $main_value)
                <div class="main-category-title">
                    <h4>
                        {{ $main_value->getAttribute('name_'.App::getLocale()) }}
                    </h4>
                </div>
                <div class="row row-start-right">
                    @foreach($main_value->subCategories as $second_value)
                        <div class="col-lg-4 col-md-6 mb-4 pull-right category-item-right">
                            <div class="card category-items">
                                <a href="/products/{{ $second_value->id }}"><img src="/images/category/images/{{ $second_value->img }}"></a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="/products/{{ $second_value->id }}">{{ $second_value->getAttribute('name_'.App::getLocale()) }}</a>
                                    </h4>
                                    <p class="card-text">{{ $main_value->getAttribute('name_'.App::getLocale()) }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            <!-- /.row -->
            @endforeach

        </div>




    </div>



@endsection