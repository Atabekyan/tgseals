{{--@extends('layouts.app')--}}
@extends('layouts.auth')

@section('content')

        <div class="sufee-login d-flex align-content-center flex-wrap">
            <div class="container">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                <div class="login-content">
                    <div class="login-logo">
                        <a href="index.html">
                            <img class="align-content" src="images/logo.png" alt="">
                        </a>
                    </div>
                    <div class="login-form">
                        <form>
                            <div class="form-group">
                                <label>Email address</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="checkbox">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    Remember Me
                                </label>

                                {{--<label>--}}
                                    {{--<input type="checkbox"> Remember Me--}}
                                {{--</label>--}}

                                <label class="pull-right">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                    {{--<a href="#">Forgotten Password?</a>--}}
                                </label>

                            </div>
                            <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                            <div class="social-login-content">

                            </div>
                            <div class="register-link m-t-15 text-center">
                                <p>Don't have account ? <a href="#"> Sign Up Here</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="form-group row">--}}
            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

            {{--<div class="col-md-6">--}}
                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"--}}
                       {{--name="email" value="{{ old('email') }}" required autofocus>--}}

                {{--@if ($errors->has('email'))--}}
                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group row">--}}
            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

            {{--<div class="col-md-6">--}}
                {{--<input id="password" type="password"--}}
                       {{--class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                {{--@if ($errors->has('password'))--}}
                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group row">--}}
            {{--<div class="col-md-6 offset-md-4">--}}
                {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" name="remember"--}}
                           {{--id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                    {{--<label class="form-check-label" for="remember">--}}
                        {{--{{ __('Remember Me') }}--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group row mb-0">--}}
            {{--<div class="col-md-8 offset-md-4">--}}
                {{--<button type="submit" class="btn btn-primary">--}}
                    {{--{{ __('Login') }}--}}
                {{--</button>--}}

                {{--@if (Route::has('password.request'))--}}
                    {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                        {{--{{ __('Forgot Your Password?') }}--}}
                    {{--</a>--}}
                {{--@endif--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection
