{{--/**--}}
{{--* Created by PhpStorm.--}}
{{--* User: Artak Atabekyan--}}
{{--* Date: 3/26/2019--}}
{{--* Time: 3:42 PM--}}
{{--*/--}}
        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TGSeals</title>
    <link rel="icon" href="/images/sidebar_logo.png"/>

    <link href="/main_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/main_assets/css/shop-homepage.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    @yield('css')

    <link rel="stylesheet" href="/css/main/style.css"/>
    <link rel="stylesheet" href="/css/main/layout.css"/>
    <link rel="stylesheet" href="/css/main/media.css"/>



    <script>
        var BASE_URL = '<?php echo e(env('APP_URL')); ?>'
    </script>

</head>
<body>
{{--style="font-family: 'Tw Cen MT';--}}
{{--src: url('../tw.eot');--}}
{{--src: url('../tw.eot?#iefix') format('embedded-opentype'),--}}
{{--url('../tw.woff') format('woff'),--}}
{{--url('../tw.ttf') format('truetype'),--}}
{{--url('../tw.svg#helveticaregular') format('svg');--}}
{{--color: white;"--}}

    <?= \App::call('App\Http\Controllers\Main\MainController@header') ?>


    <div class="container">
        <div class="row">
            <?= \App::call('App\Http\Controllers\Main\MainController@sidebar') ?>
            @yield('content')
        </div>
    </div>

    <?= \App::call('App\Http\Controllers\Main\MainController@footer') ?>



    <!-- Bootstrap core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/main_assets/vendor/jquery/jquery.min.js"></script>
    <script src="/main_assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="/js/main/language.js"></script>
    <script src="/js/main/script.js"></script>

    {{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>--}}
    {{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--}}

@yield('js')

</body>
</html>
