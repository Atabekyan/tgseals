{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: Artak Atabekyan--}}
 {{--* Date: 4/24/2019--}}
 {{--* Time: 6:07 PM--}}
 {{--*/--}}

You received a message from : {{ $name }}
<p>
    Topic: {{ $topic }}
</p>
<p>
    Name: {{ $name }}
</p>
<p>
    Company name: {{ $company_name }}
</p>
<p>
    Email: {{ $email }}
</p>
<p>
    Phone: {{ $phone }}
</p>
<p>
    Message: {{ $user_message }}
</p>
<p>
    Contact form: {{ $contact_form }}
</p>

