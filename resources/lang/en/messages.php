<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 4/2/2019
 * Time: 9:20 PM
 */

return [
//    Main Page
    'home' => 'Home page',
    'products' => 'Products',
    'products_group' => 'Product Group',
    'about_us' => 'About us',
    'materials' => 'Materials',
    'applications' => 'Applications',
    'compounds' => 'Compounds',
    'downloads' => 'Downloads',
    'contact' => 'Contact',
    'e_shop' => 'E-Shop',
    'privacy_policy' => 'Privacy policy',
    'privacy_policy_short' => 'Privacy policy',
    'imprint' => 'Imprint',
//    Contact Page
    'make_contact_with_us' => 'Make a contact with us',
    'select_topic_in_menu' => 'Please select a topic in the drop-down menu.',
    'sales' => 'Sales',
    'technical_question' => 'Technical Question',
    'quality' => 'Quality',
    'logistic' => 'Logistic',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'company_name' => 'Company Name',
    'email' => 'E-mail',
    'enter_email' => 'Enter Email',
    'please_enter_email' => 'Please enter your email',
    'phone' => 'Phone',
    'enter_phone_number' => 'Enter Phone Number ex:+374-YY-XX-XX-XX',
    'enter_phone_number_with_code' => 'Please enter your phone number with your country code',
    'message' => 'Message',
    'content' => 'Content...',
    'make_contact_with_you' => 'How can we make contact with you',
    'both' => 'Both',
    'send' => 'Send',
    'login' => 'Login',
    'register' => 'Register',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',






];