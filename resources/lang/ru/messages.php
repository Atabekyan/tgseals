<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 4/2/2019
 * Time: 9:20 PM
 */

return [
//    Main Page
    'home' => 'Главная страница',
    'products' => 'Продукты',
    'products_group' => 'Группа товаров',
    'about_us' => 'О нас',
    'materials' => 'Материалы',
    'applications' => 'Приложений',
    'compounds' => 'Соединений',
    'downloads' => 'Скачивание',
    'contact' => 'Контакты',
    'e_shop' => 'Эл. магазин',
    'privacy_policy' => 'Политика конфиденциальности',
    'privacy_policy_short' => 'Политика конф.',
    'imprint' => 'Выходные данные',
    //    Contact Page
    'make_contact_with_us' => 'Свяжитесь с нами',
    'select_topic_in_menu' => 'Пожалуйста, выберите тему в меню.',
    'sales' => 'Продажи',
    'technical_question' => 'Технический вопрос',
    'quality' => 'Качественный',
    'logistic' => 'Логистический',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'company_name' => 'Название компании',
    'email' => 'Эл. почта',
    'enter_email' => 'Введите адрес эл. почты',
    'please_enter_email' => 'Пожалуйста, введите ваш адрес эл. почты',
    'phone' => 'Номер',
    'enter_phone_number' => 'Введите номер телефона, например:+374-YY-XX-XX-XX',
    'enter_phone_number_with_code' => 'Пожалуйста, введите номер телефона с кодом страны',
    'message' => 'Сообщение',
    'content' => 'Содержание...',
    'make_contact_with_you' => 'Как мы можем связаться с вами',
    'both' => 'И то и другое',
    'send' => 'Отправлять',
    'login' => 'Вход',
    'register' => 'Регистрация',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

];