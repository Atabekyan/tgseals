<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 4/2/2019
 * Time: 9:19 PM
 */

return [
//    Main Page
    'home' => 'Գլխավոր էջ',
    'products' => 'Ապրանքներ',
    'products_group' => 'Ապրանքների խումբ',
    'about_us' => 'Մեր մասին',
    'materials' => 'Նյութեր',
    'applications' => 'Կիրառում',
    'compounds' => 'Միացություններ',
    'downloads' => 'Ներբեռնումներ',
    'contact' => 'Կապ',
    'e_shop' => 'Էլ. խանութ',
    'privacy_policy' => 'Գաղտնիության քաղաքականություն',
    'privacy_policy_short' => 'Գաղտ. քաղաքականություն',
    'imprint' => 'Արտադրություն',
    //    Contact Page
    'make_contact_with_us' => 'Կապ հաստատեք մեզ հետ',
    'select_topic_in_menu' => 'Խնդրում ենք ցանկից ընտրել թեման',
    'sales' => 'Վաճառք',
    'technical_question' => 'Տեխնիկական հարց',
    'quality' => 'Որակ',
    'logistic' => 'Լոգիստիկա',
    'first_name' => 'Անուն',
    'last_name' => 'Ազգանուն',
    'company_name' => 'Ընկերության անվանում',
    'email' => 'Էլ-հասցե',
    'enter_email' => 'Մուտքագրեք էլ-հասցեն',
    'please_enter_email' => 'Խնդրում ենք մուտքագրեք Ձեր էլ-հասցեն',
    'phone' => 'Հեռախոսահամար',
    'enter_phone_number' => 'Մուտքագրեք հեռախոսահամարը օր. +374-YY-XX-XX-XX',
    'enter_phone_number_with_code' => 'Խնդրում ենք մուտքագրեք Ձեր հեռախոսահամարը երկրի կոդով',
    'message' => 'Հաղորդագրություն',
    'content' => 'Պարունակություն...',
    'make_contact_with_you' => 'Ինչպես կարող ենք հետ կապ հաստատել Ձեզ հետ',
    'both' => 'Երկուսն էլ',
    'send' => 'Ուղարկել',
    'login' => 'Մուտք',
    'register' => 'Գրանցվել',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',






];