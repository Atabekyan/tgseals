import React, { Component } from "react";
import { Router, Route} from "react-router-dom";
import UserBasket from '../components/User/userComponents/userBasket';
import UserFavorites from '../components/User/userComponents/userHistory';
import UserInformation from '../components/User/userComponents/userInformation';
import ProductInFormation from '../components/Home/homeComponents/Product/productInformation/productInformation';
import Order from '../components/User/userComponents/order';
import history from './history';
 

class Routes extends Component {
    render(){
        return(
            //routes pass to body component inside main component
            <Router history = {history}>
                <Route exact path = '/e-shop' component= {ProductInFormation} />
                <Route path ='/e-shop/user-basket' component = {UserBasket}/>
                <Route path = '/e-shop/user-favorites' component = {UserFavorites}/>
                <Route path = '/e-shop/user-information' component = {UserInformation}/>
                <Route path = '/e-shop/order' component = {Order} />
            </Router>
        )
    }
}
export default Routes;