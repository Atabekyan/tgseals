import React, {Component} from 'react';


class ItemsForMenu extends Component{
    render(){
        return(
            <div className = "menuItem">
                <a href ={this.props.src}>{this.props.name}</a>
            </div>
        )
    }
}
export default ItemsForMenu;