import React, {Component} from 'react';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { FaChevronDown } from "react-icons/fa";
import armenian from '../../../../assets/images/am.png';
import english from '../../../../assets/images/en.png';
import russian from '../../../../assets/images/ru.png';
import '../../style/DropdownLanguageList.scss';
import {request} from '../../../helper/utils';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../../reducers/action';
import messages from '../../../../translations/messages';
import ls from 'local-storage';

class DropdownList extends Component{
    state = {
        pageLanguage : "",
        showEnglish : false,
        showArmenian : false,
        showRussian : false
    }
    
    englishLanguage = <img onClick = {() => this.english()} src = {english}/>
    armenianLanguage = <img onClick = {() => this.armenian() } src = {armenian}/>
    russianLanguage = <img onClick = {() => this.russian() } src = {russian}/>

    componentDidMount = () =>{
        
        const {languages} = this.props
        const languagesInLocalStorage = ls.get('language') && Object.keys(ls.get("language")).length !== 0  ? ls.get("language") : languages;
        if(languagesInLocalStorage.home === 'Home page'){
            this.setState({ pageLanguage : this.englishLanguage ,  showEnglish : true  })
        }
        if(languagesInLocalStorage.home === 'Главная страница'){
            this.setState({ pageLanguage : this.russianLanguage , showRussian : true  })
        }
        if(languagesInLocalStorage.home === 'Գլխավոր էջ'){
            this.setState({ pageLanguage : this.armenianLanguage , showArmenian : true})
        }

    }
    english = () => {
        let protocol =window.location.protocol
        let data={
            lang : "en"
        }
        fetch(`${protocol}/locale-change`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(response => {
               
             })
            .catch(error => error)
        this.props.Languages(messages.en);
        ls.set('language',messages.en) ;
        this.setState({ 
            pageLanguage : this.englishLanguage ,  
            showEnglish : true ,
            showArmenian : false,
            showRussian : false
          })
    
    }
    armenian = () => {
        let protocol =window.location.protocol
        fetch(`${protocol}/locale-change`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({lang : "am"})
            })
            .then(res => res.json())
            .then(response => {
               
             })
            .catch(error => error)
        this.props.Languages(messages.am);
        ls.set('language',messages.am);
        this.setState({ 
            pageLanguage : this.armenianLanguage , 
            showArmenian : true,
            showEnglish : false,
            showRussian : false
        })
    }
    russian = () => {
        let protocol =window.location.protocol
        fetch(`${protocol}/locale-change`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({lang : "ru"})
            })
            .then(res => res.json())
            .then(response => {
               
             })
            .catch(error => error)
        this.props.Languages(messages.ru);
        ls.set('language',messages.ru);
        this.setState({ 
            pageLanguage : this.russianLanguage , 
            showRussian : true,
            showEnglish : false,
            showArmenian : false,
         })
       
    }

    render(){
       
        return(
            <UncontrolledDropdown>
                <DropdownToggle >
                    {this.state.pageLanguage}
                    <FaChevronDown/>
                </DropdownToggle>
                <DropdownMenu>
                    {this.state.showEnglish ? null :<DropdownItem>
                        {this.englishLanguage }
                    </DropdownItem>}
                    {this.state.showArmenian ? null :<DropdownItem>
                       {this.armenianLanguage }
                    </DropdownItem>}
                    {this.state.showRussian ? null :<DropdownItem>
                         {this.russianLanguage}
                    </DropdownItem>}
                </DropdownMenu>
            </UncontrolledDropdown>
        )
    }
}
function mapStateToProps(state) {
    return {
        languages : state.languages
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages  
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(DropdownList)
