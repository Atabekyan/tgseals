import React ,{Component} from 'react';
import { UncontrolledCollapse, Button, CardBody, Card } from 'reactstrap';
import ItemsForMenu  from './itemsForMenu'
import { IoIosMenu } from "react-icons/io";
import '../../style/MenuWithToggle.scss'

class MenuWithToggle extends Component {
    render(){
        const {languages} = this.props
        return(
            <div className = "menuWrapper">
                <Button   id="toggler" >
                    <IoIosMenu className = 'menuIcon'/>
                </Button>
                <UncontrolledCollapse toggler="#toggler">
                <Card>
                    <CardBody>
                        <div className='menuContainer'>
                            <a href ="/">{languages.home}</a>
                            <a href ="/about-us">{languages.about_us}</a>
                            <a href ="/products">{languages.products}</a>
                            <a href ="/materials"> {languages.materials}</a>
                            <a href ="/applications">{languages.applications}</a>
                            <a href ="/e-shop">{languages.e_shop}</a>
                        </div>
                        {/* <ItemsForMenu src="http://tgseals.loc" name =  {languages.home}/> 
                        <ItemsForMenu src="http://tgseals.loc/about-us" name =  {languages.about_us} />
                        <ItemsForMenu src="http://tgseals.loc/products" name = {languages.products} />
                        <ItemsForMenu src="http://tgseals.loc/materials" name =  {languages.materials} />
                        <ItemsForMenu src="http://tgseals.loc/applications" name =  {languages.applications} />
                        <ItemsForMenu  src="http://tgseals.loc/compounds" name = {languages.compounds}  />
                        <ItemsForMenu src="http://tgseals.loc/downloads" name =  {languages.downloads} /> */}
                    </CardBody>
                </Card>
                </UncontrolledCollapse>
            </div>
        )
    }
}
export default MenuWithToggle;