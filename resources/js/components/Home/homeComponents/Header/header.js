import React,{Component} from 'react';
import '../../style/header.scss';
import 'bootstrap/dist/css/bootstrap.css';
import DropdownLanguageList from './dropdownLanguageList';
import MenuWithToggle from './menuWithToggle';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../../reducers/action';
import ls from 'local-storage';
import {Link } from 'react-router-dom';


class Header extends Component {
    


    componentDidMount = () =>{
        const {languages} = this.props
        this.props.Languages(ls.get("language") && Object.keys(ls.get("language")).length !== 0  ? ls.get("language") : languages);

    }
    render(){
        const {languages} = this.props
        return(
            <div className = "headerfixedPosition">
                <div className = 'header'>
                    <div className = "container">
                        <MenuWithToggle languages = {languages}/>  
                        <div className = 'dropwdownLanguageBar'>
                            <DropdownLanguageList/> 
                            <a href="/contacts" className = "linkForContact">
                            {languages.contact}
                            </a>
                            <Link className = "linkForContact accountLink"  to='/e-shop/user-information'>{languages.account}</Link>
                        </div>
                        
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    
    return {
        languages : state.languages
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(Header)
 