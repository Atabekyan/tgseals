import React , {Component} from 'react';


class NavBarItem extends Component {
    render(){
        return (
            <div className = {this.props.className}>
                <img src = {this.props.image}  /> 
                <a href ={this.props.src}>{this.props.name}</a>
            </div>
        )
    }
}
export default NavBarItem;