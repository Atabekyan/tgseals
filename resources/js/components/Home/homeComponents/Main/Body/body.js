import React, {Component} from 'react';
import Routes from '../../../../../routers/routes';

import '../../../style/body.scss'

class Body extends Component {
    render (){
        return(
            <div className = 'containerForBody'>
                <Routes/>
            </div>
        )
    }
}
export default Body;