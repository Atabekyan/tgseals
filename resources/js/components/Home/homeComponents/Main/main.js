import React ,{Component} from 'react';
import NavBar from './navBar';
import '../../style/main.scss';
import Body from './Body/body'

class Main extends Component{
    render (){
        return(
            <div className = 'containerForMainComponent'>
                <NavBar/>
                <Body/>
            </div>
        )
    }
}
export default Main;