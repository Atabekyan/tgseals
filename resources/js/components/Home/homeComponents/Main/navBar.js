import React, {Component} from 'react';
import mainLogo from '../../../../assets/images/sidebar_logo.png';
import '../../style/navBar.scss';
import NavBarItem from './navbarItem';
import TgIcon from '../../../../assets/images/tg_small_icon.png';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../../reducers/action';


class NavBar extends Component {
    state = {
        showTreeD: "block"
    }
    refreshInterval = setTimeout(() => {
        this.setState({showTreeD:"none"})
       
      }, 2800);
    render(){
        const {languages} = this.props
        const { showTreeD } = this.state
        return(
            <div className = "containerForNavBar">
                <div className="scene">
                    <div className="cube">
                        <div className="cube__face cube__face--front" >
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                        <div className="hide_img cube__face cube__face--back" style={{display:showTreeD}}>
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                        <div className="hide_img cube__face cube__face--right" style={{display:showTreeD}}>
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                        <div className="hide_img cube__face cube__face--left" style={{display:showTreeD}}>
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                        <div className="hide_img cube__face cube__face--top" style={{display:showTreeD}}>
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                        <div className="hide_img cube__face cube__face--bottom" style={{display:showTreeD}}>
                            <a href="/">
                                <img className="sidebar-icon-img" src="/images/sidebar_logo.png"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div className="itemsContainer">
                    <NavBarItem    
                        className ="navBarItem" 
                        image = {TgIcon} 
                        src="/about-us" 
                        name ={languages.about_us}
                    />
                    <NavBarItem 
                        className ="navBarItem"
                        image = {TgIcon}
                        src="/products" 
                        name = {languages.products}
                    />
                     <NavBarItem 
                        className ="navBarItem" 
                        image = {TgIcon} 
                        src="/materials" 
                        name = {languages.materials}
                    />
                    <NavBarItem 
                        className ="navBarItem" 
                        image = {TgIcon} 
                        src="/applications" 
                        name = {languages.applications}
                    />
                    <NavBarItem 
                        className ="navBarItem"
                        image = {TgIcon} 
                        src="/e-shop" 
                        name = {languages.e_shop}
                    />
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    
    return {
        languages : state.languages
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(NavBar)