import React,{Component,Fragment} from "react";
import MutipleSelectInput from "../../../../helper/multipleSelectInput";
import Data from "../../../../helper/productData";
import "../style/ProductDescription.scss";
import { IoMdPlay } from "react-icons/io";

class ProductDescription extends Component {
    render(){
        return(
            <div className = "productDescription">
                <MutipleSelectInput 
                    defaultState = {[]}
                    multiple = {true}
                    selectName="productGroup"
                    name = "Product Groups" 
                    productData = {this.props.productData} 
                    selectName="productGroup"
                    handleClick = {(value,name)=>this.props.handleClickMainProduct(value,name)} 
                    formClassName = "productFormStyle"
                    valueName='product_group'
                    
                />
                <IoMdPlay className="iconArrow" id = "arrowIconId"/>
                {this.props.showCompound ?
                <Fragment>
                    <MutipleSelectInput
                        defaultState = {[]}
                        multiple = {true}
                        selectName="compound"
                        name = "Compound" 
                        productData = {this.props.productCompound} 
                        handleClick = {(value,name)=>this.props.handleClick(value,name)} 
                        formClassName = "mainFormStyle"
                        valueName='compound'
                    />
                <IoMdPlay className="iconArrow" id = "arrowIconId"/>
              </Fragment>:null}
              <div className ="productImage">
                <p>{this.props.name}</p>
                {/* <img src = {`http://${this.props.image}`} alt = "img" /> */}
            </div>
        </div>
        )
    }
}
export default ProductDescription;