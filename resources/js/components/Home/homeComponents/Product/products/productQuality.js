import React, {Component,Fragment} from 'react'
import MultipleSelectInput from '../../../../helper/multipleSelectInput'
import { IoMdPlay } from "react-icons/io";
import Input from '../../../../helper/input'


class ProductQuality extends Component{
    handleSubmit(e) {
        e.preventDefault();
    }
    
    render(){ 
        const {showShaft,showHigh_h,showInner,showOuter,showHigh,showCross} = this.props
        return(
            <div>
                <div className = "productQualityContainer">
                    {showInner ?<Fragment>
                       
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            inner
                                <input style={this.props.style} name="inner" value={this.props.valueInner} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> :null}
                    {showOuter ?<Fragment>
                        <IoMdPlay className = "iconArrow" id = "arrowIconForProductQuality"/>
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            outer
                                <input style={this.props.style} name="outer" value={this.props.valueOuter} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> :null}
                    {showCross ? <Fragment>
                        <IoMdPlay className = "iconArrow" id = "arrowIconForProductQuality"/>
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            cross/thickness
                                <input style={this.props.style} name="cross" value={this.props.valueCross} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> : null}
                    {showHigh ? <Fragment>
                        <IoMdPlay className = "iconArrow" id = "arrowIconForProductQuality"/>
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            high
                                <input style={this.props.style} name="high" value={this.props.valueHigh} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> : null}
                    {showHigh_h ? <Fragment>
                        <IoMdPlay className = "iconArrow" id = "arrowIconForProductQuality"/>
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            high-H
                                <input style={this.props.style} name="high_h" value={this.props.valueHigh_h} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> : null}
                    {showShaft ?<Fragment>
                        <IoMdPlay className = "iconArrow" id = "arrowIconForProductQuality"/>
                        <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                            <label>
                            shaft
                                <input style={this.props.style} name="shaft" value={this.props.valueShaft} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                            </label>
                        </form>
                    </Fragment> :null}
                    <Fragment>
                    <IoMdPlay className="iconArrow" id = "arrowIconForProductQuality"/>
                    <form className = 'productQaulityInputStyle' onSubmit={this.handleSubmit}>
                        <label>
                        quantity
                            <input style={this.props.style} name="quantity" value={this.props.valueQuantity} placeholder={this.props.placeholder} type='text' onChange={event => this.props.handleChange(event.target.value,event.target.name)}  />
                        </label>
                    </form>
                    </Fragment>
                </div>
            </div>
        )
    }
}
export default ProductQuality