import React, {Component,Fragment} from 'react'
import MutipleSelectInput from '../../../../helper/multipleSelectInput'
import Data from '../../../../helper/productData'
import { IoMdPlay } from "react-icons/io";

class ProductDetails extends Component {
    
    render(){
        const {showType,showShore,showColor,showSpecials} = this.props
        return (
            <div className='productDetailsContainer'>
                {showType ?
                <Fragment>
                    <MutipleSelectInput
                        defaultState = {[]}
                        multiple = {true}
                        name = "Type" 
                        productData = {this.props.productType} 
                        valueName='type'
                        formClassName = "typeFormStyle"
                        selectName="type"
                        handleClick = {(value,name)=>this.props.handleClick(value,name)}
                    />
                    <IoMdPlay className="iconArrow" id = "arrowIconId"/>
                </Fragment>:null
                }
                {showShore ?
                <Fragment>
                    <MutipleSelectInput
                        productData = {this.props.productShore} 
                        valueName='shore'
                        defaultState = {[]}
                        multiple = {true}
                        name = "Shore"  
                        selectName="shore"
                        handleClick = {(value,name)=>this.props.handleClick(value,name)}
                        formClassName = "typeFormStyle"
                    />
                    <IoMdPlay className="iconArrow" id = "arrowIconId"/>
                </Fragment>:null}
                {showColor ? 
                <Fragment>
                    <MutipleSelectInput
                        defaultState = {[]}
                        multiple = {true}
                        name = "Color" 
                        productData = {this.props.productColor}
                        valueName='color_specials'
                        formClassName = "typeFormStyle"
                        selectName="color"
                        handleClick = {(value,name)=>this.props.handleClick(value,name)}
                    />
                    <IoMdPlay className="iconArrow" id = "arrowIconId"/>
                </Fragment>:null
                }
                {showSpecials ? 
                <MutipleSelectInput
                    defaultState = {[]}
                    multiple = {true}
                    productData = {this.props.productSpecial}
                    valueName='special'
                    name = "Specials"
                    formClassName = "typeFormStyle"
                    selectName="specials"
                    handleClick = {(value,name)=>this.props.handleClick(value,name)}
                />:null
                }
            </div>
        )
    }
}
export default ProductDetails;