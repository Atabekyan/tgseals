import React, {Component} from 'react';
import ProductDescription from '../products/productDescription';
import ProductDetails from '../products/productDetails';
import ProductQuality from '../products/productQuality';
import './productInformation.scss';
import Button from '../../../../helper/button';
import history from '../../../../../routers/history';
import ls from 'local-storage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Data from "../../../../helper/productData";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../../../reducers/action';
import {ProductMainData,ForSearchProductGroup,UserId} from '../../../../../reducers/action';
// import { Spinner } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';


class ProductInformation extends Component {
    state={
        quantity:'1',
        cross:0,
        
        high:0,
        outer:0,
        inner:0,
        high_h:0,
        shaft:0,
        productMain:{},
        showType:true,
        showShore:true,
        showCompound:true,
        showColor:true,
        showSpecial:true,
        showDimension:true,
        errorText:'',
        showShaft:true,
        showHigh_h:true,
        showInner:true,
        showOuter:true,
        showHigh:true,
        showCross:true,
        forSearchProductGroup:{},
        user_id: "",
        loading:false
    }
    _isMounted = false;
    addToBasket = ()=>{

        let basketData =  ls.get('selectedProduct') ? ls.get('selectedProduct') : {}
        const {languages} = this.props;

        if(this.state && this.state.productGroup){
            let choosedData={
                user_id: this.state.user_id,
                product_group: this.state.productGroup?this.state.productGroup:null,
                compound: this.state.compound ? this.state.compound:null,
                thickness: this.state.cross ? this.state.cross:null,
                color_specials: this.state.color ? this.state.color : null,
                dimension: this.state.dimension ? this.state.dimension:null,
                inner: this.state.inner ? this.state.inner :null,
                outer:this.state.outer ? this.state.outer:null ,
                high:this.state.high ? this.state.high :null,
                quantity: this.state.quantity ? this.state.quantity :1,
                shore: this.state.shore ? this.state.shore :null, 
                special: this.state.specials ? this.state.specials:null ,
                type: this.state.type ? this.state.type :null,
                shaft:this.state.shaft ? this.state.shaft :null,
                high_H:this.state.high_h ? this.state.high_h :null
            }

            let protocol =window.location.protocol
            const {forSearchProductGroup}=this.state

            for(let i=0;i<Object.values(forSearchProductGroup).length;i++){
                let data=Object.values(forSearchProductGroup)[i]
                if(data.product_group===choosedData.product_group&&choosedData.compound===data.compound&&
                    choosedData.dimension===data.dimension&&(data.shore ? parseFloat(choosedData.shore)===parseFloat(data.shore):true)&&choosedData.color_specials===data.color_specials&&data.type===choosedData.type&&
                    data.special===choosedData.special&&(data.high_H_min ?parseFloat(data.high_H_min)<parseFloat(choosedData.high_H)&&parseFloat(choosedData.high_H)<parseFloat(data.high_H_max):true) &&
                    (data.shaft_min? parseFloat(data.shaft_min)<parseFloat(choosedData.shaft)&&parseFloat(choosedData.shaft)<parseFloat(data.shaft_max):true) &&
                     (data.inner_min ? parseFloat(data.inner_min)<parseFloat(choosedData.inner)&&parseFloat(choosedData.inner)<parseFloat(data.inner_max):true)&&
                     (data.outer_min ? parseFloat(data.outer_min)<parseFloat(choosedData.outer)&&parseFloat(choosedData.outer)<parseFloat(data.outer_max):true)&&
                    (data.high_min?parseFloat(data.high_min)<parseFloat(choosedData.high)&&parseFloat(choosedData.high)<parseFloat(data.high_max):true)&&
                    (data.thickness_cross_section_min ? parseFloat(data.thickness_cross_section_min)<parseFloat(choosedData.thickness)&&
                    parseFloat(choosedData.thickness)<parseFloat(data.thickness_cross_section_max):true)){
                        ls.set('selectedProduct',basketData)
                                    this.setState({errorText:''})
                                    fetch( `${protocol}/api/product/basket`,{
                                    method: "POST",
                                    headers: {"Content-Type": "application/json"},
                                    body: JSON.stringify(choosedData)
                                    })
                                    .then(res => res.json())
                                    .then(response => response)
                                    .catch(error => error);
                                    toast.success(languages.basketMessage)
                        break;
                    }else{
                        this.setState({errorText:languages.requestError})

                    }
            }

        } else {
            toast.error("Please choose product")
            this.setState({productGroup:''})
        }
    }
    componentDidMount=()=> {
      
        this._isMounted = true;
        if( Object.values(this.props.productMainData).length===0||Object.values(this.props.productMainData).length===undefined){
            this.setState({loading:true})
            let protocol =window.location.protocol
            fetch(`${protocol}/api/product`)
                .then(response => response.json()  
                )
                .then(result => {
                    this.setState({loading:false})
                    if(this._isMounted) { 
                        let productMain = result.products_for_search.filter( (ele, ind) => ind === result.products_for_search.findIndex( elem => elem.product_group === ele.product_group))
                        let filterData = productMain.filter(data=>{
                        return (data.product_group!=="product_group")
                        })
                        let mainproductafetrfilter = filterData.filter(data=>{
                            return (data.product_group!==null)
                        })
                        this.props.ProductMainData(mainproductafetrfilter)
                        this.props.ForSearchProductGroup(result.products_for_search)
                        this.props.UserId( result.user_id)
                        this.setState({productMain:mainproductafetrfilter,forSearchProductGroup:result.products_for_search,user_id: result.user_id})
                    }
                }
               
            )
            .catch(e =>   e);
        }else{
            this.setState({loading:false})
            //propsic vercni 
          
            const {productMainData,productSearchData,userId} = this.props
            this.setState({productMain:productMainData,forSearchProductGroup:productSearchData,user_id: userId})
        }
       
    }
    componentDidUpdate=(prevProps,prevState)=> {
        
        if(this.state.productGroup!==prevState.productGroup){
            this.setState({showType:true})
            var {forSearchProductGroup,productGroup}=this.state
            var selectedProductData = Object.values(forSearchProductGroup).filter(data=>{
                return productGroup===data.product_group
            })
            Object.values(selectedProductData).map(data=>{
                data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
            })
            var productCompound = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.compound === ele.compound)).filter(data=>data.compound!==null)
            
            if(productCompound === undefined || productCompound.length == 0){
                this.setState({showCompound:false})
                var productType = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.type === ele.type)).filter(data=>data.type!==null)
                
                if(productType === undefined || productType.length == 0){
                    this.setState({showType:false})
                    let productShore = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.shore === ele.shore)).filter(data=>data.shore!==null)
                       
                    if(productShore === undefined || productShore.length == 0){
                           this.setState({showShore:false})
                            let productColor = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.color_specials === ele.color_specials)).filter(data=>data.color_specials!==null)
                            
                            if(productColor === undefined || productColor.length == 0){ 
                                this.setState({showColor:false })
                                let productSpecial = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.special === ele.special)).filter(data=>data.special!==null)
                                
                                if(productSpecial === undefined || productSpecial.length == 0){
                                    this.setState({showSpecial:false})
                                    // let productDimension = selectedProductData.filter( (ele, ind) => ind === selectedProductData.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                                    
                                    // if(productDimension === undefined || productDimension.length == 0){
                                    //    return this.setState({showDimension:false})
                                    // }else {
                                    //     this.setState({showDimension:true,productDimension:productDimension})
                                    // }
                                }else{
                                    this.setState({
                                        showSpecial:true,
                                        productSpecial:productSpecial
                                    })
                                }

                            }else{
                                this.setState({
                                    productColor:productColor,
                                    showColor:true
                                })
                            }

                        }else{
                            this.setState({
                                productShore:productShore,
                                showShore:true
                            })
                        }

                } else {
                    this.setState({
                        showType:true,
                        productType:productType})
                }
            }
            return this.setState({
                showCompound:true,
                productCompound:productCompound,
                selectedProductData:selectedProductData
             })
        }

        if(this.state.compound!==prevState.compound){
            const {selectedProductData,compound,type} =this.state
            var selectedCompound = Object.values(selectedProductData).filter(data=>{
                return compound===data.compound
            })
            Object.values(selectedCompound).map(data=>{
                data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
            })
            let productType = selectedCompound.filter( (ele, ind) => ind === selectedCompound.findIndex( elem => elem.type === ele.type)).filter(data=>data.type!==null)
                
            if(productType === undefined || productType.length == 0){
                    this.setState({showType:false})
                    let productShore = selectedCompound.filter( (ele, ind) => ind === selectedCompound.findIndex( elem => elem.shore === ele.shore)).filter(data=>data.shore!==null)
                       
                    if(productShore === undefined || productShore.length == 0){
                           this.setState({showShore:false})
                            let productColor = selectedCompound.filter( (ele, ind) => ind === selectedCompound.findIndex( elem => elem.color_specials === ele.color_specials)).filter(data=>data.color_specials!==null)
                            
                            if(productColor === undefined || productColor.length == 0){ 
                                this.setState({showColor:false })
                                let productSpecial = selectedCompound.filter( (ele, ind) => ind === selectedCompound.findIndex( elem => elem.special === ele.special)).filter(data=>data.special!==null)
                                
                                if(productSpecial === undefined || productSpecial.length == 0){
                                    
                                    this.setState({showSpecial:false})
                                    // Object.values(selectedCompound).map(data=>{
                                    //     data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                                    //     data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                                    //     data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                                    //     data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                                    //     data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                                    //     data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
                                    // })
                                    // console.log(selectedCompound,'jjj')
                                    // let productDimension = selectedCompound.filter( (ele, ind) => ind === selectedCompound.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                                    
                                    // if(productDimension === undefined || productDimension.length == 0){
                                    //    return this.setState({showDimension:false})
                                    // }else {
                                    //     this.setState({showDimension:true,productDimension:productDimension})
                                    // }
                                }else{
                                    this.setState({
                                        showSpecial:true,
                                        productSpecial:productSpecial
                                    })
                                }
                            }else{
                                this.setState({
                                    productColor:productColor,
                                    showColor:true
                                })
                            }
                        }else{
                            this.setState({
                                productShore:productShore,
                                showShore:true
                            })
                        }
            }else{
                this.setState({
                    productType:productType,
                    showType:true
                 })
            }
        }
        if(this.state.type!==prevState.type){
            const { selectedProductData,type,compound} =this.state
            var selectedType = Object.values(selectedProductData).filter(data=>{
                return (type===data.type&&compound===data.compound)
            })
            Object.values(selectedType).map(data=>{
                data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
            })
            let productShore = selectedType.filter( (ele, ind) => ind === selectedType.findIndex( elem => elem.shore === ele.shore)).filter(data=>data.shore!==null)
            
            if(productShore === undefined || productShore.length == 0){
                this.setState({showShore:false})
                 let productColor = selectedType.filter( (ele, ind) => ind === selectedType.findIndex( elem => elem.color_specials === ele.color_specials)).filter(data=>data.color_specials!==null)
                 
                 if(productColor === undefined || productColor.length == 0){ 
                     this.setState({showColor:false })
                     var productSpecial = selectedType.filter( (ele, ind) => ind === selectedType.findIndex( elem => elem.special === ele.special)).filter(data=>data.special!==null)
                     
                     if(productSpecial === undefined || productSpecial.length == 0){
                         this.setState({showSpecial:false})
                         //let productDimension = selectedType.filter( (ele, ind) => ind === selectedType.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                        //  Object.values(selectedType).map(data=>{
                        //     data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                        //     data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                        //     data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                        //     data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                        //     data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                        //     data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
                        // })
                        //  if(productDimension === undefined || productDimension.length == 0){
                        //     return this.setState({showDimension:false})
                        //  }else {
                        //      this.setState({showDimension:true,productDimension:productDimension})
                        //  }
                     }else{
                         this.setState({
                             showSpecial:true,
                             productSpecial:productSpecial
                         })
                     }
                 }else{
                     this.setState({
                         productColor:productColor,
                         showColor:true
                     })
                 }
             }else{
                 this.setState({
                     productShore:productShore,
                     showShore:true
                 })
             }
        }
        
        if(this.state.shore!==prevState.shore){
            const {selectedProductData,shore,type,compound,special} =this.state
            var selectedColor = Object.values(selectedProductData).filter(data=>{
                if(type){
                    return shore===data.shore&& compound===data.compound && type===data.type
                }
                return (shore===data.shore&& compound===data.compound)
            })
            Object.values(selectedColor).map(data=>{
                data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
            })
            let productColor = selectedColor.filter( (ele, ind) => ind === selectedColor.findIndex( elem => elem.color_specials === ele.color_specials)).filter(data=>data.color_specials!==null)
            
            if(productColor === undefined || productColor.length == 0){ 
                
                this.setState({showColor:false })
                let productSpecial = selectedColor.filter( (ele, ind) => ind === selectedColor.findIndex( elem => elem.special === ele.special)).filter(data=>data.special!==null)
                
                if(productSpecial === undefined || productSpecial.length == 0  ){
                    this.setState({showSpecial:false})
                    // Object.values(selectedColor).map(data=>{
                    //     data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                    //     data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                    //     data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                    //     data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                    //     data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                    //     data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
                    // })
                   
                    // let productDimension = selectedColor.filter( (ele, ind) => ind === selectedColor.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                    
                    // if(productDimension === undefined || productDimension.length == 0){
                    //    return this.setState({showDimension:false})
                    // }else {
                    //     this.setState({showDimension:true,productDimension:productDimension})
                    // }
                }else{
                    this.setState({
                        showSpecial:true,
                        productSpecial:productSpecial
                    })
                }
            }else{
                this.setState({
                    productColor:productColor,
                    showColor:true
                })
            }
             
        }
        
        if(this.state.color!==prevState.color){
            const {selectedProductData,shore,type,compound,color,special} =this.state
            var selectedSpecials = Object.values(selectedProductData).filter(data=>{
                if(shore&&type&&compound&&color){
                   return (shore===data.shore&& compound===data.compound && type===data.type  && color===data.color_specials)
                }
                else if(compound&&type&&color){
                    return (compound===data.compound && type===data.type &&color===data.color_specials)
                }
                else if(shore&&compound&&color){
                    return (shore===data.shore&& compound===data.compound && color===data.color_specials)
                }
                else if(compound&&color){
                    return (compound===data.compound && color===data.color_specials)
                }
                else if(compound&&shore){
                    return (compound===data.compound && shore===data.shore)
                }
                else if(compound&&type){
                    return (compound===data.compound && type===data.type)
                }
                
            })
            Object.values(selectedSpecials).map(data=>{
                data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
            })
                let productSpecial = selectedSpecials.filter( (ele, ind) => ind === selectedSpecials.findIndex( elem => elem.special === ele.special)).filter(data=>data.special!==null)
                
                if(productSpecial === undefined || productSpecial.length == 0){
                    this.setState({showSpecial:false})
                    // Object.values(selectedSpecials).map(data=>{
                    //     data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                    //     data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                    //     data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                    //     data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                    //     data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                    //     data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
                    // })
                    // let productDimension = selectedSpecials.filter( (ele, ind) => ind === selectedSpecials.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                    
                    // if(productDimension === undefined || productDimension.length == 0){
                    //    return this.setState({showDimension:false})
                    // }else {
                    //     this.setState({showDimension:true,productDimension:productDimension})
                    // }
                }else{
                    this.setState({
                        showSpecial:true,
                        productSpecial:productSpecial
                    })
                }
            } 
            
            if(this.state.specials!==prevState.specials){
                const {selectedProductData,shore,type,compound,color,sprcial} =this.state
                var selectedDemisions = Object.values(selectedProductData).filter(data=>{
                    
                    if(shore&&type&&compound&&color&&sprcial){
                        return (shore===data.shore&& compound===data.compound && type===data.type  && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(compound&&type&&color&&shore){
                        return (compound===data.compound && type===data.type &&color===data.color_specials&& shore===data.shore)
                    }
                    
                    if(compound&&type&&color&&sprcial){
                        return (compound===data.compound && type===data.type &&color===data.color_specials&& sprcial===data.sprcial)
                    }
                    
                    if(shore&&compound&&color){
                        return (shore===data.shore&& compound===data.compound && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(type&&compound&&color){
                        return (type===data.type&& compound===data.compound && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(type&&shore&&color){
                        return (type===data.type&& shore===data.shore && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(type&&compound&&shore){
                        return (type===data.type&& compound===data.compound && shore===data.shore && sprcial===data.sprcial)
                    }
                    
                    if(compound&&color){
                        return (compound===data.compound && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(compound&&shore){
                        return (compound===data.compound && shore===data.shore && sprcial===data.sprcial)
                    }
                    
                    if(compound&&type){
                        return (compound===data.compound && type===data.type&& sprcial===data.sprcial)
                    }
                    
                    if(type&&color){
                        return (type===data.type && color===data.color_specials && sprcial===data.sprcial)
                    }
                    
                    if(type&&shore){
                        return (type===data.type && shore===data.shore && sprcial===data.sprcial)
                    }
                    
                    if(shore&&color){
                        return (shore===data.shore && color===data.color_specials && sprcial===data.sprcial)
                    }
                   
                })
                Object.values(selectedDemisions).map(data=>{
                    data.shaft_min ? this.setState({showShaft:true}) :  this.setState({showShaft:false})
                    data.high_H_min ? this.setState({showHigh_h:true}) :  this.setState({showHigh_h:false})
                    data.inner_min ? this.setState({showInner:true}) :  this.setState({showInner:false})
                    data.outer_min ? this.setState({showOuter:true}) :  this.setState({showOuter:false})
                    data.high_min ? this.setState({showHigh:true}) :  this.setState({showHigh:false})
                    data.thickness_cross_section_min ? this.setState({showCross:true}) :  this.setState({showCross:false})
                })
                    let productDimension = selectedDemisions.filter( (ele, ind) => ind === selectedDemisions.findIndex( elem => elem.dimension === ele.dimension)).filter(data=>data.dimension!==null)
                        
                    if(productDimension === undefined || productDimension.length == 0){
                           return this.setState({showDimension:false})
                        }else {
                            this.setState({showDimension:true,productDimension:productDimension})
                        }
                    
                } 
        }
    handleClick =(value,name) =>{
        switch(name) {
            case 'compound':
                this.setState({
                    [name]:value,
                    productType:{},
                    productShore:{},
                    productDimension:{},
                    productColor:{},
                    productSpecial:{},
                    type:null,
                    color:null,
                    shore:null,
                    dimention:null,
                    specials:null,
                })
            break;
            case 'type':
                this.setState({
                    [name]:value,
                    productShore:{},
                    productDimension:{},
                    productColor:{},
                    productSpecial:{},
                    color:null,
                    shore:null,
                    dimention:null,
                    specials:null,
                })
            break;
            case 'shore':
                this.setState({
                    [name]:value,
                    productDimension:{},
                    productColor:{},
                    productSpecial:{},
                    color:null,
                    dimention:null,
                    specials:null,
                })
            break;
            case 'color':
                this.setState({
                    [name]:value,
                    productDimension:{},
                    productSpecial:{},
                    dimention:null,
                    specials:null,
                })
            break;
            case 'specials':
                this.setState({
                    [name]:value,
                    productDimension:{},
                    dimention:null})
            break;
            default:
              // code block
          }
        

    }
    handleChange = (value,name) => {
       
        this.setState({[name]:value})
       
        
    }
    makeOrder = () => {

        const {languages} = this.props;
        
        if(this.state && this.state.productGroup){
            let choosedData={
                user_id: this.state.user_id,
                product_group: this.state.productGroup?this.state.productGroup:null,
                compound: this.state.compound ? this.state.compound:null,
                thickness: this.state.cross ? this.state.cross:null,
                color_specials: this.state.color ? this.state.color : null,
                dimension: this.state.dimension ? this.state.dimension:null,
                inner: this.state.inner ? this.state.inner :null,
                outer:this.state.outer ? this.state.outer:null ,
                high:this.state.high ? this.state.high :null,
                quantity: this.state.quantity ? this.state.quantity :1,
                shore: this.state.shore ? this.state.shore :null, 
                special: this.state.specials ? this.state.specials:null ,
                type: this.state.type ? this.state.type :null,
                shaft:this.state.shaft ? this.state.shaft :null,
                high_H:this.state.high_h ? this.state.high_h :null
            }
            
            const {forSearchProductGroup}=this.state
            
            for(let i=0;i<Object.values(forSearchProductGroup).length;i++){
                let data=Object.values(forSearchProductGroup)[i]
               
                if(data.product_group===choosedData.product_group&&choosedData.compound===data.compound&&
                    choosedData.dimension===data.dimension&&(data.shore ? parseFloat(choosedData.shore)===parseFloat(data.shore):true)&&choosedData.color_specials===data.color_specials&&data.type===choosedData.type&&
                    data.special===choosedData.special&&(data.high_H_min ?parseFloat(data.high_H_min)<parseFloat(choosedData.high_H)&&parseFloat(choosedData.high_H)<parseFloat(data.high_H_max):true) &&
                    (data.shaft_min? parseFloat(data.shaft_min)<parseFloat(choosedData.shaft)&&parseFloat(choosedData.shaft)<parseFloat(data.shaft_max):true) &&
                     (data.inner_min ? parseFloat(data.inner_min)<parseFloat(choosedData.inner)&&parseFloat(choosedData.inner)<parseFloat(data.inner_max):true)&&
                     (data.outer_min ? parseFloat(data.outer_min)<parseFloat(choosedData.outer)&&parseFloat(choosedData.outer)<parseFloat(data.outer_max):true)&&
                    (data.high_min?parseFloat(data.high_min)<parseFloat(choosedData.high)&&parseFloat(choosedData.high)<parseFloat(data.high_max):true)&&
                    (data.thickness_cross_section_min ? parseFloat(data.thickness_cross_section_min)<parseFloat(choosedData.thickness)&&
                    parseFloat(choosedData.thickness)<parseFloat(data.thickness_cross_section_max):true)){
                      
                        let protocol =window.location.protocol
                        this.setState({errorText:''})
                    fetch(`${protocol}/api/product/order`,{
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(choosedData)
                    })
                    .then(res => res.json())
                    .then(response => { 
                        ls.set('orderChechResponse',response)
                        ls.set('selectedOrderData',choosedData)
                        history.push('/e-shop/order')
                     })
                    .catch(error => error)
                      
                                 
                        break;
                }else{
                        this.setState({errorText:languages.requestError})

                    }
            }
        } else {
            toast.error("Please choose product")
            this.setState({productGroup:''})
        }
    }
   
    
    handleClickMainProduct = (value,name)=> {
        this.setState({
            [name]:value,
            productCompound:{},
            productType:{},
            productShore:{},
            productDimension:{},
            productColor:{},
            productSpecial:{},
            compound:null,
            type:null,
            color:null,
            shore:null,
            dimention:null,
            specials:null,
            showShaft:true,
            showHigh_h:true,
            showInner:true,
            showOuter:true,
            showHigh:true,
            showCross:true,
            cross:0,
            high:0,
            outer:0,
            inner:0,
            high_h:0,
            shaft:0,
            
        })
    }
    render ()   {
        const {
            productMain,
            productCompound,
            productType,
            productShore,
            productDimension,
            productColor,
            productSpecial
        }=this.state
        return(
            <div className = "productContainer"> 
                    <LoadingOverlay
                        active={this.state.loading}
                        spinner
                        text='Loading ...'
                        styles={{
                            wrapper: {
                                
                              }
                        }}
                        >
                    </LoadingOverlay>
                
           
                <ToastContainer />
                <ProductDescription
                    handleClickMainProduct = {this.handleClickMainProduct}
                    handleClick = { this.handleClick } 
                    productData = {this.state && productMain ? productMain : {} }
                    productCompound={this.state && productCompound ? productCompound:{}}
                    showCompound={this.state.showCompound}
                />
                <ProductDetails
                    showType = {this.state.showType}
                    showShore = {this.state.showShore}
                    showColor={this.state.showColor}
                    showSpecials={this.state.showSpecial}
                    productType={this.state && productType ? productType:{}}
                    productColor={this.state && productColor ? productColor:{}}
                    productSpecial={this.state && productSpecial ? productSpecial:{}}
                    handleClick = { this.handleClick } 
                    productShore={this.state && productShore ?productShore :{}}
                />
                <ProductQuality
                    showDimension={this.state.showDimension}
                    handleChange = { this.handleChange}
                    handleClick = { this.handleClick } 
                    data = {this.state && this.state.compound ? Data : {} }
                    valueCross = {this.state   ? this.state.cross : 0}
                    valueQuantity = {this.state  ? this.state.quantity : 1 }
                    valueInner = {this.state  ? this.state.inner : 0 }
                    valueOuter ={this.state  ? this.state.outer : 0 }
                    valueHigh={this.state  ? this.state.high: 0}
                    valueHigh_h={this.state  ? this.state.high_h: 0}
                    valueShaft={this.state  ? this.state.shaft: 0}
                    productDimension = {this.state && productDimension ? productDimension :{}}
                    showShaft={this.state.showShaft}
                    showHigh_h ={this.state.showHigh_h}
                    showInner ={this.state.showInner}
                    showOuter = {this.state.showOuter}
                    showHigh ={this.state.showHigh}
                    showCross ={this.state.showCross}
                />
                <p className='errorAboutProduct'>{this.state.errorText}</p>
                <div className="orderButtonContainer">
                    <Button 
                        callback={this.addToBasket} 
                        name="Add to basket"
                        className="addToBasket"
                    />
                    <Button 
                        callback = {this.makeOrder} 
                        name="Order"
                        className="makeOrder"
                        />
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    
    return {
        languages : state.languages,
        productMainData : state.productMainData,
        productSearchData : state.productSearchData,
        userId : state.userId
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages,
            ProductMainData,
            ForSearchProductGroup,
            UserId
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(ProductInformation)