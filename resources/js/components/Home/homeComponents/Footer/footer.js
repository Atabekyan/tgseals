import React, {Component} from 'react';
import mainLogo from '../../../../assets/images/main_logo.png';
import NavBarItem from '../Main/navbarItem';
import FooterIcon from '../../../../assets/images/footer_icon.png';
import '../../style/footer.scss';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../../reducers/action';

class Footer extends Component {
    render(){
        const {languages} = this.props
        return(
            <footer className = "footerBody">
            <div className = 'footerContent'>
                <div className = 'footerLinks'>
                    <div className = "footerLinks-1">
                        <NavBarItem 
                            
                            image = {FooterIcon} 
                            src="/e-shop" 
                            name = {languages.e_shop}
                        />
                         <NavBarItem
                         
                            image = {FooterIcon} 
                            src="/products" 
                            name =  {languages.products}
                        />
                    </div>
                    <div className = "footerLinks-2">
                        <NavBarItem
                       
                            image = {FooterIcon} 
                            src="/about-us" 
                            name = {languages.about_us}
                        />
                        <NavBarItem
                           
                            image = {FooterIcon} 
                            src ="/contacts" 
                            name = {languages.contact}
                        />
                    </div>
                    <div className = "footerLinks-3">
                        <NavBarItem
                      
                            image = {FooterIcon} 
                            src="/applications" 
                            name = {languages.applications}
                        />
                       <NavBarItem
                            image = {FooterIcon} 
                            src="/privacy-policy" 
                            name = {languages.privacy_policy}
                        />
                    </div>
                </div>
                <div className = 'footerLogo'>
                    <img src = {mainLogo}/>
                </div>
            </div>
            <div className = 'webDeveloper'>
                <p>© Copyright 2019</p>
                <p>All rights reserved. Powered by 
                    <b>
                        <a target = '_blank' href = 'https://www.facebook.com/deltaSoftwareArmenia/'>Delta Software</a>
                    </b>
                </p>
            </div>
            </footer>
        )    
    }
}
function mapStateToProps(state) {
    
    return {
        languages : state.languages
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(Footer)
