import React, {Component,Fragment} from 'react';
import { FaTrashAlt } from "react-icons/fa";

class TableBodyWithDeleteIcon extends Component {

    render(){
        const {data} = this.props
        return(
            <Fragment>
                <tr>
                    <th scope="row"></th>
                    <td> </td>
                    <td> </td>
                    <td> </td>
                    <td >
                        <div className='divForIcons'>
                        <FaTrashAlt onClick = {this.deleteRow}/>{this.props.action}
                        </div>
                    </td>
                </tr>
            </Fragment>
        )
    }
}
export default TableBodyWithDeleteIcon;