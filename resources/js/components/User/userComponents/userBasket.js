import React , {Component,Fragment} from 'react';
import Button from '../../helper/button';
import { Table } from 'reactstrap';
import { FaTrashAlt } from "react-icons/fa";
import ls from 'local-storage';
import Modal from '../../helper/mainModal.js';
import history from '../../../routers/history';
 
class UserBasket extends Component {
    state = {
        basketData: ls.get('selectedProduct') ? ls.get('selectedProduct') : {},
        user_id:''
    }
     componentDidMount = () => {
        let protocol =window.location.protocol
        fetch(`${protocol}/api/basket/info`)
            .then(response => response.json())
            .then(result => {
            this.setState({basketData:result.basket ,user_id:result.user_id})
        })
        .catch(e => console.log(e));
    }
     
     buyAllProducts = (toggle,allData) => {
        console.log("aaaaaaaaa",allData)
        let protocol =window.location.protocol  
        fetch(`${protocol}/api/basket/add/order_all`,{
             method: "POST",
             headers: {"Content-Type": "application/json"},
             body: JSON.stringify(allData)
            })
             .then(res => res.json())
             .then(response => { 
             console.log(response)
             
            })
            .catch(error => error)
        ls.set('selectedProduct',{})
        this.setState({basketData:{}})
        toggle()
    }
    deleteData =(product)=> {
        const {basketData} = this.state
        Object.values(basketData).forEach((data,index)=>{
        if(data.id===product.id){
            basketData.splice(index, 1);
             //post request send id (procust.id)
             let protocol =window.location.protocol  
             fetch(`${protocol}/api/basket/delete_one`,{
             method: "POST",
             headers: {"Content-Type": "application/json"},
             body: JSON.stringify(product.id)
            })
             .then(res => res.json())
             .then(response => { 
             console.log(response)
             
            })
            .catch(error => error)
            ls.set('selectedProduct',basketData)
            this.setState({basketData})
        }
        })
       
    }
    deleteRow = (toggle,product) => {
        this.deleteData(product)
        toggle()

    }
    buyProduct = (toggle,data) => {
        let protocol =window.location.protocol
        fetch(`${protocol}/api/product/order`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(response => {
                ls.set('orderChechResponse',response)
                ls.set('selectedOrderData',data)
                history.push('/e-shop/order')
             })
            .catch(error => error)
       
        toggle()
      
    }
    deleteAllProducts=(toggle,data)=>{
        let allid=Object.values(data).map(values=>{
            return values.id
        })
        const {user_id} = this.state
        //post
        let protocol =window.location.protocol  
        fetch(`${protocol}/api/basket/delete_all`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(allid,user_id)
           })
            .then(res => res.json())
            .then(response => { 
            console.log(response)
            this.setState({basketData:{}})
            
           })
           .catch(error => error)
        toggle()
    }
    render(){
        const {basketData} = this.state
        return(
            <Fragment>
                <h2 className='pageHeader'>Basket</h2>
                <Table>
                    <thead className = "tableHeader">
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Compound</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {basketData && Object.values(basketData).length !==0 ? Object.values(basketData).map((data,index) => {
                            return (
                        <tr key={index}>
                            <th scope="row">{index+1}</th>
                           
                            <td>{data.product_group} </td>
                            <td>{data.compound}</td>
                            <td>{data.quantity}  </td>
                            <td >
                                <div className='divForIcons'>
                                < Modal
                                    mainDivClassName = "confirmation"
                                    editData={(toggle,rowData=data)=>this.deleteRow(toggle,rowData)}
                                    toggleName={<FaTrashAlt />} 
                                    save= "YES"
                                    cancel="NO"
                                    className="deleteAccount"
                                >
                                    <p className='conformationText' >Do you want to delete {data.name} ?</p>
                                </ Modal>
                                < Modal
                                    mainDivClassName = "confirmation"
                                    editData={(toggle,rowData=data)=>this.buyProduct(toggle,rowData)}
                                    toggleName="Buy"
                                    save= "YES"
                                    cancel="NO"
                                    className="deleteAccount"
                                    toggleClassName="buttonBuy"
                                >
                                    <p className='conformationText' >Do you want to buy {data.name} ?</p>
                                </ Modal>
                                 
                                </div>
                            </td>
                        </tr>
                            )
                        }):[]
                        }
                    </tbody>
                </Table>
                <div className='basketButtonsContainer'>
                    < Modal
                        mainDivClassName = "confirmation"
                        editData={(toggle,rowData=basketData)=>this.deleteAllProducts(toggle,rowData)}
                        toggleName="Delete All"
                        save= "YES"
                        cancel="NO"
                        className="deleteAccount "
                        toggleClassName="buttonBuyAll deleteAllButton"
                    >                
                        <p className='conformationText' >Do you want to delete all products from basket ?</p>
                    </ Modal>
                    < Modal
                        mainDivClassName = "confirmation"
                        editData={(toggle,rowData=basketData)=>this.buyAllProducts(toggle,rowData)}
                        toggleName="Buy All"
                        save= "YES"
                        cancel="NO"
                        className="deleteAccount"
                        toggleClassName="buttonBuyAll"
                    >                
                        <p className='conformationText' >Do you want to buy all products ?</p>
                    </ Modal>
                </div>
                
            </Fragment>
        )
    }
}
export default UserBasket;