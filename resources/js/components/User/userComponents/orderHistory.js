import React, {Component,Fragment} from 'react';
import TableComponent from '../../helper/tableComponent'
import TableBody from '../../helper/tableBody'


class OrderHistory extends Component{
    render(){
        return(
            <Fragment>
                <h1>Order History</h1>
                <TableComponent>
                    <TableBody/>
                </TableComponent>
            </Fragment>

        )
    }
}
export default OrderHistory;