import React , {Component,Fragment} from 'react';
import { Table } from 'reactstrap';

class UserFavorites extends Component {
    state = {
        basketData: {}
    }
     componentDidMount = () => {
        let protocol =window.location.protocol
        fetch(`${protocol}/api/order/info`)
            .then(response => response.json())
            .then(result => {
            this.setState({basketData:result.order})
        })
        .catch(e => console.log(e));
     }
    render(){
        const {basketData} = this.state
    
        return(
            <Fragment> 
            <h2 className='pageHeader'>History</h2>
                <Table>
                    <thead className = "tableHeader">
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Compound</th>
                            <th>Price per product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        {basketData && Object.values(basketData).length !==0 ? Object.values(basketData).map((data,index) => {
                            return (
                        <tr key={index}>
                            <th scope="row">{index+1}</th>
                           
                            <td>{data.product_group} </td>
                            <td>{data.compound}</td>
                            <td >{data.price}</td>
                            <td>{data.quantity}  </td>
                            <td>{data.price? parseInt(data.price)*parseInt(data.quantity):''}  </td>
                            
                        </tr>
                            )
                        }):[]
                        }
                    </tbody>
                </Table>
            </Fragment>

        )
    }
}
export default UserFavorites;