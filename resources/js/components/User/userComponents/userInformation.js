import React,{Component} from 'react';
import { FaShoppingCart } from "react-icons/fa";
import { FaList} from "react-icons/fa";
import MainModal from '../../helper/modalForEditing';
import User from '../../../assets/images/avatar.png';
import '../style/userInformation.scss';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {Languages} from '../../../reducers/action';
import history from '../../../routers/history';
import Input from '../../helper/input';
// import request from '../../helper/utils';
import Modal from '../../helper/mainModal.js';
import { FaEyeSlash,FaEye } from "react-icons/fa";

class UserInformation extends Component {
    state = {
        value : null,
        userInformation : {},
        updatedUserData : {},
        request:false,
        showPassword:false,
        passwordInputType:"password"
    }
    data ={
        firstName: "Anahit",
        companyName: "DeltaSoft",
        email: "anahitavetisyan1993@gmail.com",
        phone: "055311503",
        userName: "A.A",
        password: "199325"

    }
    componentDidMount = () => {
        let protocol =window.location.protocol
        fetch(`${protocol}/api/user`)
            .then(response => response.json())
            .then(result => this.setState({userInformation:result,user_id:result.user.id}))
            .catch(e => console.log(e));
    }
    handleChange = (value,name) => {
        let newData = Object.assign({},this.state.updatedUserData);
         newData={
             [name]:value
         }
        this.setState({updatedUserData:newData});
        
       
    }
    userFavoritePage = () => {
        history.push('/e-shop/user-favorites')
    }
    userBasketPage = () => {
        history.push ('/e-shop/user-basket')
    }
    editData = (close) => {
        const {updatedUserData,userInformation} = this.state
        let updateData=userInformation.user
        // console.log(updateData,'jjj')
        Object.keys(updatedUserData).map(key => {
            Object.values(updatedUserData).map(value => {
                Object.keys(updateData).map(data=>{
                    if(data === key){
                        updateData[key]=value
                        
                    }
                    if(key==='password'){
                        let addPassword=Object.assign({},updateData)
                        addPassword[key]=value
                        updateData=addPassword
                    }
                    let newUserData=Object.assign({},this.state.userInformation)
                    newUserData['user']=updateData
                    // newUserData['id']=updateData.id
                   
                    //post request
                    let protocol =window.location.protocol  
                    fetch(`${protocol}/api/user/edit`,{
                    method: "POST",
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify(newUserData)
                    })
                    .then(res => res.json())
                    .then(response => response)
                    .catch(error => error)
                    newUserData['user']=updateData
                    this.setState({userInformation:newUserData})
                    let DeletePassword = Object.assign({},userInformation)
                    DeletePassword['user']['password']=''
                    this.setState({userInformation:DeletePassword})
                })
              
            })
            
        })
       close()
    }
    close = (toggle)=> {
        this.setState({updatedUserData:{}})
        toggle()
        
    }
   
    deleteAccount = (toggle)=>{
        const {user_id} = this.state
        let protocol =window.location.protocol  
        let id={
            user_id
        }
        fetch(`${protocol}/api/user/delete`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(id)
            })
            .then(res => res.json())
            .then(response => { 
                return response
            })
            .catch(error => error)
        // window.location.pathname="/" 
        toggle()
    }
    exitFromAccount = () => {
        window.location="/logout"
    }
    hidePassword = () => {
        this.setState({showPassword:false,passwordInputType:"password"})
    }
    showPassword = () => {
        this.setState({showPassword:true,passwordInputType:"text"})
    }
    render(){
        const {languages} = this.props;
        const {userInformation} = this.state
        return(
            <div className = 'userInformationContainer'>
                <div className = 'favoritesAndBasket'>
                    <FaList onClick = {this.userFavoritePage}/>
                    <FaShoppingCart onClick = {this.userBasketPage}/>
                </div>  
                <div className = 'userInformationHeader'>
                    <img src = {User}/>
                    <div className = 'fieldForName'>
                        <div><span> {languages.first_name}:</span> <span className="userInformationStyle"> {userInformation.user ?`${userInformation.user.first_name} ` :''} </span>
                            <MainModal
                                className = "userInformationModal" 
                                save = {languages.save}
                                cancel = {languages.cancel}
                                editData={this.editData}
                                close = {this.close}
                                closeHeader ={this.close}
                                modalTitle= {languages.first_name} 
                            >
                                <Input   
                                    placeholder = {languages.first_name} 
                                    type = 'text'
                                    defaultState = {userInformation.user ?`${userInformation.user.first_name}  ` :''}
                                    inputName = "first_name"
                                    changeCallback={this.handleChange}
                                    className="userInformationModalInput"
                                    // name = "firstName"
                                />
                             
                            </MainModal>
                        </div>
                        <div><span> {languages.surname}:</span> <span className="userInformationStyle"> {userInformation.user ? userInformation.user.surname :''} </span>
                            <MainModal
                                className = "userInformationModal" 
                                save = {languages.save}
                                cancel = {languages.cancel}
                                editData={this.editData}
                                close = {this.close}
                                closeHeader ={this.close}
                                modalTitle= {languages.surname} 
                            >
                                <Input   
                                    placeholder = {languages.surname} 
                                    type = 'text'
                                    defaultState = {userInformation.user ? userInformation.user.surname :''}
                                    inputName = "surname"
                                    changeCallback={this.handleChange}
                                    className="userInformationModalInput"
                                    // name = "firstName"
                                />
                             
                            </MainModal>
                        </div>
                        <div><span>{languages.company_name}:</span>  <span className="userInformationStyle">  {userInformation.user ? userInformation.user.company_name:''}</span>
                            <MainModal
                                className = "userInformationModal"
                                save = {languages.save}
                                cancel = {languages.cancel}
                                editData={this.editData}
                                close = {this.close}
                                closeHeader ={this.close}
                                modalTitle={languages.company_name}
                            >
                                <Input
                                    className="userInformationModalInput"
                                    placeholder = {languages.company_name}
                                    type = 'text'
                                    defaultState = {userInformation.user ? userInformation.user.company_name:''}
                                    inputName = "company_name"
                                    changeCallback={this.handleChange}
                                />
                            </MainModal>
                        </div>
                        <div><span>VAT:</span>  <span className="userInformationStyle">  {userInformation.user ? userInformation.user.vat:''}</span>
                            <MainModal
                                className = "userInformationModal"
                                save = {languages.save}
                                cancel = {languages.cancel}
                                editData={this.editData}
                                close = {this.close}
                                closeHeader ={this.close}
                                modalTitle='Vat'
                            >
                                <Input
                                    className="userInformationModalInput"
                                    placeholder = 'Vat'
                                    type = 'text'
                                    defaultState = {userInformation.user ? userInformation.user.vat:''}
                                    inputName = "vat"
                                    changeCallback={this.handleChange}
                                />
                            </MainModal>
                        </div>
                    </div>             
                </div>
                <div className = 'userInformationMain'>
                    <div><span>{languages.email}:</span>  <span className="userInformationStyle"> {userInformation.user ? userInformation.user.email:''} </span>
                        <MainModal
                            save = {languages.save}
                            cancel = {languages.cancel}
                            editData={this.editData}
                            close = {this.close}
                            closeHeader ={this.close}
                            className = "userInformationModal"
                            modalTitle= {languages.email}

                        >
                            <Input
                                className="userInformationModalInput"
                                placeholder  = {languages.email}
                                type = 'text'
                                defaultState = {userInformation.user ? userInformation.user.email : ''}
                                inputName = "email"
                                changeCallback={this.handleChange}
                                
                            />
                        </MainModal>
                    </div>
                    <div><span>{languages.phone}: </span> <span className="userInformationStyle"> {userInformation.user ? userInformation.user.phone :''}</span>
                        <MainModal
                            className = "userInformationModal"
                            save = {languages.save}
                            cancel = {languages.cancel}
                            editData={this.editData}
                            close = {this.close}
                            closeHeader ={this.close}
                            modalTitle = {languages.phone}
                        >
                            <Input
                                className="userInformationModalInput"
                                placeholder  = {languages.phone}
                                type = 'text'
                                defaultState = {userInformation.user ? userInformation.user.phone : ''}
                                inputName = "phone"
                                changeCallback={this.handleChange}
                                
                            />
                        </MainModal>
                    </div>
                    <div><span>{languages.address} :</span>  <span className="userInformationStyle"> {userInformation.user ? userInformation.user.address:''} </span>
                        <MainModal
                            modalTitle={languages.address}
                            className = "userInformationModal"
                            save = {languages.save}
                            cancel = {languages.cancel}
                            editData={this.editData}
                            close = {this.close}
                            closeHeader ={this.close}
                        >
                            <Input
                                className="userInformationModalInput"
                                placeholder  = {languages.address}
                                type = 'text'
                                defaultState = {userInformation.user ? userInformation.user.address:''}
                                inputName = "address"
                                changeCallback={this.handleChange}
                            />
                        </MainModal>
                    </div>
                    <div><span>{languages.password}: </span> <span className="userInformationStyle"> </span>
                        <MainModal
                            modalTitle = {languages.password}
                            saveChnages = {this.editName}
                            save = {languages.save}
                            cancel = {languages.cancel}
                            editData={this.editData}
                            close = {this.close}
                            closeHeader = {this.close}
                            className = "userInformationModal"
                        >
                            <Input
                                placeholder  = {languages.password}
                                className="userInformationModalInputForPassword"
                                type = {this.state.passwordInputType}
                                defaultState = {userInformation.user ? userInformation.user.password : ''}
                                inputName ='password'
                                changeCallback={this.handleChange}
                                nameAfterInput={this.state.showPassword ?<FaEye  onClick = {this.hidePassword} className='showPasswordIcon'/> : <FaEyeSlash onClick = {this.showPassword} className='showPasswordIcon'/>}
                            />
                        </MainModal>
                    </div>
                </div>
                <div className="exitButtonsContainer">
                     
                     < Modal
                            mainDivClassName = "confirmation"
                            toggleClassName="toggleClassNameExitModal"
                            editData={(toggle )=>this.exitFromAccount(toggle )}
                            toggleName="Exit from account"
                            // save={<a className='exitButton' href="http://tgseals.loc">Yes</a>}
                            save='Yes'
                            cancel="NO"
                            className="deleteAccount"
                            
                        >
                            <p className='conformationText' > Do you want to exit from account?</p>
                    </ Modal>
                    < Modal
                            mainDivClassName = "confirmation"
                            toggleClassName="toggleClassNameExitModal"
                            className="deleteAccount"
                            editData={(toggle )=>this.deleteAccount(toggle )}
                            toggleName="Delete Account"
                            // save={<a className='exitButton' href="http://tgseals.loc">Yes</a>}
                            save='Yes'
                            cancel="NO"
                            
                        >
                            <p className='conformationText'  >Do you want to delete account?</p>
                    </ Modal>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    
    return {
        languages : state.languages
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            Languages
        },
        dispatch
    );
}   
        
export default connect(mapStateToProps,mapDispatchToProps)(UserInformation)
