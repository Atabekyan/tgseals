import React,{Component,Fragment} from 'react';
import Button from '../../helper/button';
import ls from 'local-storage';
import { Table } from 'reactstrap';
import { FaRegEdit } from "react-icons/fa";
import history from '../../../routers/history';
import Modal from '../../helper/mainModal.js'

class Order extends Component {
    state = {
        orderData: ls.get('selectedOrderData') ? ls.get('selectedOrderData') : {},
        stockInfo:'',
        textColor: '',
        quantity:''
    }
     componentDidMount = () => {
        
        let orderChechResponse = ls.get('orderChechResponse') ? ls.get('orderChechResponse') :{}
        let basketFromLS = ls.get('selectedOrderData') ? ls.get('selectedOrderData') :{}
            if(orderChechResponse.order_in_stock&&orderChechResponse.quantity){
                this.setState({stockInfo:'We have such product in stock',textColor:'green',quantity:orderChechResponse.user_quantity,orderData:orderChechResponse.order_in_stock,user_id:basketFromLS.user_id})
            }
            if(orderChechResponse.order_in_stock&&!orderChechResponse.quantity){
                this.setState({stockInfo:`We have such product in stock but quantity is equal ${orderChechResponse.order_in_stock.quantity}`,textColor:'rgb(182, 167, 31)',quantity:orderChechResponse.user_quantity,orderData:orderChechResponse.order_in_stock,user_id:basketFromLS.user_id})
            }
            if(orderChechResponse.order_out_stock){
                this.setState({stockInfo:'We dont have such product in stock but you can order it',textColor:'rgb(175, 73, 73)',quantity:orderChechResponse.user_quantity,orderData:orderChechResponse.order_out_stock})
            }
     }
     EditQuantity = (toggle,product) => {
        const {quantityEdited}=this.state
        let orderChechResponse = ls.get('orderChechResponse') ? ls.get('orderChechResponse') :{}
        if(orderChechResponse.order_in_stock&&parseInt(orderChechResponse.order_in_stock.quantity)<parseInt(quantityEdited)){
            this.setState({stockInfo:`We have such product in stock but quantity is equal ${orderChechResponse.order_in_stock.quantity}`,textColor:'rgb(182, 167, 31)'})
        }if(orderChechResponse.order_in_stock&&parseInt(orderChechResponse.order_in_stock.quantity)>parseInt(quantityEdited)){
            this.setState({stockInfo:'We have such product in stock',textColor:'green'})
        }
        
       this.setState({quantity:quantityEdited})
        toggle()
    }
    sendOrder=(protocol,addQuantity)=>{
        fetch(`${protocol}/api/product/order/submit`,{
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(addQuantity)
            })
            .then(res => res.json())
            .then(response => { })
            
            .catch(error => error)
            ls.remove('selectedOrderData')
            history.push('/e-shop')
    }
    submit = (toggle,data) => {
        let protocol =window.location.protocol
        let addQuantity=Object.assign({},data)
       if(this.state.user_id){
        addQuantity['user_quantity'] = this.state.quantity  
        addQuantity['user_id'] = this.state.user_id
        this.sendOrder(protocol,addQuantity)
       }else{
        addQuantity['user_quantity'] = this.state.quantity  
        
        this.sendOrder(protocol,addQuantity)
       }
            toggle()
    }
    handleChangeQuantity=(value,name)=>{
        this.setState({[name]:value})
    }
    cancelOrder=()=> {
        ls.remove('selectedOrderData')
        history.push('/e-shop')

    }
    round=(value, precision)=> {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }
    render() {
        const {orderData} = this.state
        
        return(
            <Fragment>
                <h2 className='pageHeader'>Order</h2>
                <Table>
                    <thead className = "tableHeader">
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Compound</th>
                            <th>Price Per product</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <th scope="row">1</th>
                            <td>{orderData.product_group} </td>
                            <td>{orderData.compound}</td>
                            <td>{orderData.price}</td>
                            <td>{this.state.quantity}  </td>
                            <td>{orderData.price ? this.round(parseFloat(orderData.price) * parseFloat(this.state.quantity),5):''}  </td>
                            <td >
                                <div className='divForIcons'>
                                < Modal
                                    mainDivClassName = "confirmation"
                                    editData={(toggle,rowData=orderData)=>this.EditQuantity(toggle,rowData)}
                                    toggleName={<FaRegEdit />} 
                                    save= "YES"
                                    cancel="NO"
                                    className="deleteAccount"
                                >
                                    <p className='conformationText' >Do you want to edit quantity of {orderData.product_group} ?</p>
                                    <form className = 'productQuantity' onSubmit={this.handleSubmit}>
                                        <label>
                                        quantity
                                            <input className='editQuantity' name="quantityEdited" defaultValue={this.state.quantity} placeholder='quantity' type='number' onChange={event => this.handleChangeQuantity(event.target.value,event.target.name)}  />
                                        </label>
                                    </form>
                                </ Modal>
                                  
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </Table>
                <p style={{color:this.state.textColor}}>{this.state.stockInfo}</p>
                <div className='orderButtonsContainer'>
                    <Button
                        name='cancel'
                        className="deleteAccount"
                        callback={this.cancelOrder}
                    />
                    < Modal
                        mainDivClassName = "confirmation"
                        editData={(toggle,rowData=orderData)=>this.submit(toggle,rowData)}
                        toggleName="Submit" 
                        save= "YES"
                        cancel="NO"
                        className="deleteAccount"
                        toggleClassName="buttonBuyAll"
                    >                
                        <p className='conformationText' >Do you want to buy {orderData.product_group} ?</p>
                    </ Modal>
                </div>
                
            </Fragment>
        )
    }
}
export default Order;