import React,{Component} from 'react';


class MultipleSelectInput extends Component {
    
 
 
    render(){
        return(
            <form onSubmit={this.handleSubmit} className={this.props.formClassName}>
            <label>
            {this.props.name}
              <select multiple={this.props.multiple} className = {this.props.selectClassName} name={this.props.selectName}    onChange={(event)=>this.props.handleClick(event.target.value,event.target.name)}  >
            
              {!this.props.productData ? [] : Object.values(this.props.productData).map((data,index) => {          
                        return( 
                            
                <option key = {index} value={data[this.props.valueName]}>{data[this.props.valueName]}</option>
              )})
            }
                
                
              </select>
            </label>
          </form>
        )
    }
}
export default MultipleSelectInput;