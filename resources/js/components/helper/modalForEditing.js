import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import '../../components/User/style/modalForEditing.scss';
import { IoMdCreate } from "react-icons/io";

class MainModal  extends React.Component {
    state = {
        modal: false
    };
 

  toggle= () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  
  render() {
    
    return (
      <div>
        <Button   onClick={this.toggle}><IoMdCreate className="changeData"/></Button>
        <Modal 
            isOpen={this.state.modal} 
            toggle={this.toggle} 
            className={this.props.className}
            backdrop="static"
          >
          <ModalHeader toggle={()=>this.props.close(this.toggle)}>{this.props.modalTitle}</ModalHeader>
          <ModalBody>
              {this.props.children}
          </ModalBody>
          <ModalFooter>
            <Button     onClick={()=>this.props.editData(this.toggle)}>{this.props.save}</Button>{' '}
            <Button   onClick={()=>this.props.close(this.toggle)}>{this.props.cancel}</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default MainModal;