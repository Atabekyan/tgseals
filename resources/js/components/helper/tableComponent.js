import React,{Component} from 'react';
import { Table } from 'reactstrap';
import '../User/style/tableComponent.scss';

class TableComponent extends Component {
 
  render() {
    return (
     
      <Table>
        <thead className = {this.props.className}>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>#</th>
                <th>#</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          {this.props.children}
        </tbody>
      </Table>
      
    );
  }
}
export default TableComponent;