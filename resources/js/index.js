import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import { Provider } from 'react-redux';
import reducer from   './reducers/reducers'
import { createStore,applyMiddleware } from 'redux';
import thunk  from 'redux-thunk';
import history from './routers/history';
import { Router} from "react-router-dom";


const store=createStore(reducer,applyMiddleware(thunk))
ReactDOM.render(
    <Provider store={store} >
        <Router history = {history}>
            <App />
        </Router>
    </Provider>
, document.getElementById('app'))