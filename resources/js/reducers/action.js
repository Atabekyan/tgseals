export function Languages(value){
    return{
        type:"CHANGE_LANGUAGE",
        value
    }
};
export function ProductMainData(value){
    return{
        type:"CHANGE_PRODUCT_DATA",
        value
    }
};
export function ForSearchProductGroup(value){
    return{
        type:"CHANGE_SEARCH_PRODUCT_DATA",
        value
    }
};
export function UserId(value){
    return{
        type:"CHANGE_USER_ID",
        value
    }
};
