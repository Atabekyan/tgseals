import { combineReducers } from 'redux';
import messages from '../translations/messages';


const initialState = messages.en

function languages(state = initialState, action) {
    switch (action.type){
        case "CHANGE_LANGUAGE":
        return action.value
        default :
        return state 
    }
};
function productMainData(state = {}, action) {
    switch (action.type){
        case "CHANGE_PRODUCT_DATA":
        return action.value
        default :
        return state 
    }
};

function productSearchData(state = {}, action) {
    switch (action.type){
        case "CHANGE_SEARCH_PRODUCT_DATA":
        return action.value
        default :
        return state 
    }
};

function userId(state = '', action) {
    switch (action.type){
        case "CHANGE_USER_ID":
        return action.value
        default :
        return state 
    }
};
const reducer = combineReducers({languages,productMainData,productSearchData,userId});
      
export default reducer;