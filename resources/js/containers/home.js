import React,{Component } from 'react';
import Header from '../components/Home/homeComponents/Header/header';
import Main from '../components/Home/homeComponents/Main/main';
import './styleContainers/home.scss';
import Footer from '../components/Home/homeComponents/Footer/footer'

class Home extends Component {
    
    render(){
        return(
            <div className = 'containerForHomeComponent'>
                <Header/>
                <div className='containerMain'>
                    <Main/>
                </div>
                <Footer/>
            </div>
        
        )
    }

}
export default Home;