<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('page_name');
            $table->string('header_en')->nullable()->default(null);
            $table->string('header_am')->nullable()->default(null);
            $table->string('header_ru')->nullable()->default(null);
            $table->text('content_ru')->nullable()->default(null);
            $table->text('content_en')->nullable()->default(null);
            $table->text('content_am')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
