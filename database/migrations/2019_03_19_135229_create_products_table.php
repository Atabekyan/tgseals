<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type_img')->nullable()->default(null);
            $table->string('name_en');
            $table->string('name_am');
            $table->string('name_ru');
            $table->string('type');
            $table->text('description_ru')->default(null);
            $table->text('description_en')->default(null);
            $table->text('description_am')->default(null);
            $table->string('img')->nullable()->default(null);
            $table->string('main_category_id')->nullable()->default(null);
            $table->string('second_category_id');
            $table->string('unique_id')->unique()->nullable()->default(NULL);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
