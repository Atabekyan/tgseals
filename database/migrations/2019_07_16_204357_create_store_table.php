<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_for_search', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_group')->nullable()->default(null);
            $table->string('type')->nullable()->default(null);
            $table->string('compound')->nullable()->default(null);
            $table->string('shore')->nullable()->default(null);
            $table->string('special')->nullable()->default(null);
            $table->string('color_specials')->nullable()->default(null);
            $table->string('dimension')->nullable()->default(null);
            $table->string('inner_min')->nullable()->default(null);
            $table->string('inner_max')->nullable()->default(null);
            $table->string('outer_min')->nullable()->default(null);
            $table->string('outer_max')->nullable()->default(null);
            $table->string('thickness_cross_section_min')->nullable()->default(null);
            $table->string('thickness_cross_section_max')->nullable()->default(null);
            $table->string('high_min')->nullable()->default(null);
            $table->string('high_max')->nullable()->default(null);
            $table->string('high_H_min')->nullable()->default(null);
            $table->string('high_H_max')->nullable()->default(null);
            $table->string('shaft_min')->nullable()->default(null);
            $table->string('shaft_max')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store');
    }
}
