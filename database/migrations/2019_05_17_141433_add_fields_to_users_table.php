<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname')->after('name')->nullable()->default(null);
            $table->string('company_name')->after('surname')->nullable()->default(null);
            $table->string('phone')->after('company_name')->nullable()->default(null);
            $table->string('vat')->after('phone')->nullable()->default(null);
            $table->string('address')->after('vat')->nullable()->default(null);
            $table->string('avatar')->after('role')->nullable()->default(null);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company_name');
            $table->dropColumn('phone');
            $table->dropColumn('avatar');
        });
    }
}
