<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable()->default(null);
            $table->string('product_group')->nullable()->default(null);
            $table->string('type')->nullable()->default(null);
            $table->string('compound')->nullable()->default(null);
            $table->string('shore')->nullable()->default(null);
            $table->string('special')->nullable()->default(null);
            $table->string('color_specials')->nullable()->default(null);
            $table->string('dimension')->nullable()->default(null);
            $table->string('inner')->nullable()->default(null);
            $table->string('outer')->nullable()->default(null);
            $table->string('thickness')->nullable()->default(null);
            $table->string('high')->nullable()->default(null);
            $table->string('high_H')->nullable()->default(null);
            $table->string('shaft')->nullable()->default(null);
            $table->string('price')->nullable()->default(null);
            $table->string('quantity')->nullable()->default(null);
            $table->string('is_available')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basket');
    }
}
