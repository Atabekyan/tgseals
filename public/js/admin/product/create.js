/**
 * Created by Artak Atabekyan on 3/20/2019.
 */
jQuery(document).ready(function($) {
    "use strict";

    CKEDITOR.replace('editor_am');
    CKEDITOR.replace('editor_ru');
    CKEDITOR.replace('editor_en');

    CKEDITOR.on('instanceReady',function(){
        $('.cke_button__link').remove();
        $('.cke_button__about').remove();
        $('a').attr('target' , '_blank');
    });

    // Upload Type IMG
    var imgUploadElement = $('.img-upload');

    imgUploadElement.removeClass('hidden');
    imgUploadElement.find('input').attr('name' , 'type_img');

    $('#upload-img').on('change' , function(e){
        var type_img = e.target.files.item(0);
        $('.img-name').text(type_img.name);
    });


    // Product image upload start
    var f = $('#upload-product-images')[0];
    var formData = new FormData(f);

    $('#upload-product-images').on('change', function (event) {

        var uploadedFiles = event.target.files;

        $.each(uploadedFiles , function(index,value){
            formData.append('image' + Math.random() , value , value.name);

            var reader = new FileReader();

            reader.readAsDataURL(value);
            reader.onloadend = function () {
                var src = reader.result;
                $('.product-images').append('' +
                    '<div class="image-box portlet">' +
                    // '<div class="portlet-header">'+
                    '<img alt="' + value.name + '" src="' + src + '" class="single-image">' +
                    '<i class="fa fa-remove delete-image"></i>' +
                    // '</div>'+
                    '</div>')
            };
        });
    });
    // image upload end

    $( ".product-images").sortable();

    //end sort IMG

    // delete image start
    $(document).on('click', '.delete-image', function () {

        var img_name = $(this).parent().find('img').attr('alt');
        var img_div = $(this).parent();

        for (var pair of formData.entries()) {
            if(pair[1].name == img_name){
                formData.delete(pair[0]);
                img_div.remove();
            }
        }
    });
    // delete image end

    // create product start
    $('.create-product-button').on('click' , function(e){
        e.preventDefault();

        var form = $(this).closest('form');

        $.ajax({
            url : BASE_URL + '/admin/product/upload-image' ,
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(response){
                if (response.success){

                    var newArr = [];
                    $.each($( ".product-images img") , function (i,v) {
                        var name = v.alt;
                        newArr.push(response.response[name]);
                    });

                    form.find('input#uploaded-product-images').val(newArr.join());
                }

                // fill CKeditor input values
                for(var i in CKEDITOR.instances) {
                    CKEDITOR.instances[i].element.setValue(CKEDITOR.instances[i].getData())
                }

                var valid = true;
                $.each(form.find('input[required]') , function (i,v) {
                    if ($(this).val().length == 0){
                        valid = false;
                    }
                });

                if (valid){
                    form.submit();
                }

            }

        })

    });

    // create product end
    $('#add-select-category').on('change' , function(){

        var option = $(this).find('option:selected');
        var categoryId = option.val();
        var optional = option.data('optional');
        var categoryOptionalElem = $('.category-optional');

        if (optional) {

            categoryOptionalElem.html('');

            var optionalContent = option.data('optional-content');

            $.each(optionalContent , function(i,v){

                var options = JSON.parse(v.value_ru);
                var optionsHtml = '';

                $.each(options , function(i,v){
                    optionsHtml += "<option value='"+v+"'>"+v+"</option>"
                });

                var selectLabel = "<label>"+v.key_ru+"</label>";
                var selectHtml = selectLabel + "<select name='optional["+v.id+"]' class='form-control'>"+optionsHtml+"</select>";

                $('.category-optional').append(selectHtml);
            });

            $.each($('option[data-parent-category='+categoryId+']') , function(i,v){

                var optional = $(this).data('optional');

                if (optional){

                    var optionalContent = $(this).data('optional-content');

                    $.each(optionalContent , function(i,v){

                        var options = JSON.parse(v.value_ru);
                        var optionsHtml = '';

                        $.each(options , function(i,v){
                            optionsHtml += "<option value='"+v+"'>"+v+"</option>"
                        });

                        var selectLabel = "<label>"+v.key_ru+"</label>";
                        var selectHtml = selectLabel + "<select name='optional["+v.id+"]' class='form-control'>"+optionsHtml+"</select>";

                        $('.category-optional').append(selectHtml);
                    });
                }

            });
        } else {
            categoryOptionalElem.html('');
        }
    });

});