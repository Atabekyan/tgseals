/**
 * Created by Artak Atabekyan on 4/16/2019.
 */
jQuery(document).ready(function($) {
    "use strict";

    CKEDITOR.replace('editor_am');
    CKEDITOR.replace('editor_ru');
    CKEDITOR.replace('editor_en');

    CKEDITOR.on('instanceReady',function(){
        $('.cke_button__link').remove();
        $('.cke_button__about').remove();
        $('a').attr('target' , '_blank');
    });

    for(var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].setData(CKEDITOR.instances[i].element.getAttribute('value'));
    }

    for(var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].element.setValue(CKEDITOR.instances[i].getData())
    }

    var imgUploadElement = $('.img-upload');

    imgUploadElement.removeClass('hidden');
    imgUploadElement.find('input').attr('name' , 'type_img');

    $('#upload-type-img').on('change' , function(e){
        var type_img = e.target.files.item(0);
        $('.type-img-name').text(type_img.name);
    });

    // var title = $('.description').data('book-description');
    // CKEDITOR.instances.description.setData(title);

});