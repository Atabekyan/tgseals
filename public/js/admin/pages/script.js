/**
 * Created by Artak Atabekyan on 4/8/2019.
 */

jQuery(document).ready(function($) {
    "use strict";

    CKEDITOR.replace('editor_am');
    CKEDITOR.replace('editor_ru');
    CKEDITOR.replace('editor_en');

    CKEDITOR.on('instanceReady',function(){
        $('.cke_button__link').remove();
        $('.cke_button__about').remove();
        $('a').attr('target' , '_blank');
    });

    for(var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].setData(CKEDITOR.instances[i].element.getAttribute('value'));
    }

    for(var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].element.setValue(CKEDITOR.instances[i].getData())
    }

    $('#upload-img').on('change' , function(e){
        var img = e.target.files.item(0);
        $('.img-name').text(img.name);
    });

});