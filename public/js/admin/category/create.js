/**
 * Created by Artak Atabekyan on 3/17/2019.
 */

jQuery(document).ready(function($) {
    "use strict";

    // check if category is parent allow svg upload as img
    $('.select-category').on('change' , function(){
        var value = $(this).find('option:selected').data('img');
        var imgUploadElement = $('.img-upload');

        if (value == true){
            imgUploadElement.removeClass('hidden');
            imgUploadElement.find('input').attr('name' , 'img');
        } else {
            imgUploadElement.addClass('hidden');
            imgUploadElement.find('input').attr('name' , '');
        }
    });

    $('#upload-img').on('change' , function(e){
        var img = e.target.files.item(0);
        $('.img-name').text(img.name);
    });




});