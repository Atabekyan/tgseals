<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 5/24/2019
 * Time: 1:26 PM
 */

/**
 * Pages About us routes
 */
Route::group(['prefix' => 'about-us'], function () {
    Route::get('/', [
        'as' => 'about-us.getIndex',
        'uses' => 'Admin\PageController@indexAboutUs'
    ]);
//    Create Img
    Route::get('/create-img',[
        'as' => 'about-us-img.getCreate',
        'uses' => 'Admin\PageController@createAboutUsImg'
    ]);
    Route::post('/create-img',[
        'as' => 'about-us-img.postCreate',
        'uses' => 'Admin\PageController@storeAboutUsImg'
    ]);
//    Create Header
    Route::get('/create-header', [
        'as' => 'about-us-header.getCreate',
        'uses' => 'Admin\PageController@createAboutUsHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'about-us-header.postCreate',
        'uses' => 'Admin\PageController@storeAboutUsHeader'
    ]);
//    Create Content
    Route::get('/create-content', [
        'as' => 'about-us-content.getCreate',
        'uses' => 'Admin\PageController@createAboutUsContent'
    ]);
    Route::post('/create-content', [
        'as' => 'about-us-content.postCreate',
        'uses' => 'Admin\PageController@storeAboutUsContent'
    ]);
//    Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'about-us-header.getUpdate',
        'uses' => 'Admin\PageController@editAboutUsHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'about-us-header.postUpdate',
        'uses' => 'Admin\PageController@updateAboutUsHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'about-us-content.getUpdate',
        'uses' => 'Admin\PageController@editAboutUsContent'
    ]);
    Route::post('/update-content', [
        'as' => 'about-us-content.postUpdate',
        'uses' => 'Admin\PageController@updateAboutUsContent'
    ]);
//            Delete
    Route::get('/about-us/{id}', [
        'as' => 'about-us.getDelete',
        'uses' => 'Admin\PageController@destroyAboutUs'
    ]);
});

///////////////////////////////
Route::get('/img-delete/{id}', [
    'as' => 'img-delete.getDelete',
    'uses' => 'Admin\PageController@destroyImg'
]);
//////////////////////////////

/**
 * Pages Applications routes
 */
Route::group(['prefix' => 'applications'], function () {
    Route::get('/', [
        'as' => 'applications.getIndex',
        'uses' => 'Admin\PageController@indexApplications'
    ]);
    //    Create Img
    Route::get('/create-img',[
        'as' => 'applications-img.getCreate',
        'uses' => 'Admin\PageController@createApplicationsImg'
    ]);
    Route::post('/create-img',[
        'as' => 'applications-img.postCreate',
        'uses' => 'Admin\PageController@storeApplicationsImg'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'applications-header.getCreate',
        'uses' => 'Admin\PageController@createApplicationsHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'applications-header.postCreate',
        'uses' => 'Admin\PageController@storeApplicationsHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'applications-content.getCreate',
        'uses' => 'Admin\PageController@createApplicationsContent'
    ]);
    Route::post('/create-content', [
        'as' => 'applications-content.postCreate',
        'uses' => 'Admin\PageController@storeApplicationsContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'applications-header.getUpdate',
        'uses' => 'Admin\PageController@editApplicationsHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'applications-header.postUpdate',
        'uses' => 'Admin\PageController@updateApplicationsHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'applications-content.getUpdate',
        'uses' => 'Admin\PageController@editApplicationsContent'
    ]);
    Route::post('/update-content', [
        'as' => 'applications-content.postUpdate',
        'uses' => 'Admin\PageController@updateApplicationsContent'
    ]);
//            Delete
    Route::get('/applications/{id}', [
        'as' => 'applications.getDelete',
        'uses' => 'Admin\PageController@destroyApplications'
    ]);
});

/**
 * Pages Compounds routes
 */
Route::group(['prefix' => 'compounds'], function () {
    Route::get('/', [
        'as' => 'compounds.getIndex',
        'uses' => 'Admin\PageController@indexCompounds'
    ]);
    //    Create Img
    Route::get('/create-img',[
        'as' => 'compounds-img.getCreate',
        'uses' => 'Admin\PageController@createCompoundsImg'
    ]);
    Route::post('/create-img',[
        'as' => 'compounds-img.postCreate',
        'uses' => 'Admin\PageController@storeCompoundsImg'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'compounds-header.getCreate',
        'uses' => 'Admin\PageController@createCompoundsHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'compounds-header.postCreate',
        'uses' => 'Admin\PageController@storeCompoundsHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'compounds-content.getCreate',
        'uses' => 'Admin\PageController@createCompoundsContent'
    ]);
    Route::post('/create-content', [
        'as' => 'compounds-content.postCreate',
        'uses' => 'Admin\PageController@storeCompoundsContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'compounds-header.getUpdate',
        'uses' => 'Admin\PageController@editCompoundsHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'compounds-header.postUpdate',
        'uses' => 'Admin\PageController@updateCompoundsHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'compounds-content.getUpdate',
        'uses' => 'Admin\PageController@editCompoundsContent'
    ]);
    Route::post('/update-content', [
        'as' => 'compounds-content.postUpdate',
        'uses' => 'Admin\PageController@updateCompoundsContent'
    ]);
//            Delete
    Route::get('/compounds/{id}', [
        'as' => 'compounds.getDelete',
        'uses' => 'Admin\PageController@destroyCompounds'
    ]);
});

/**
 * Pages Contact routes
 */
Route::group(['prefix' => 'contact'], function () {
    Route::get('/', [
        'as' => 'contact.getIndex',
        'uses' => 'Admin\PageController@indexContact'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'contact-header.getCreate',
        'uses' => 'Admin\PageController@createContactHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'contact-header.postCreate',
        'uses' => 'Admin\PageController@storeContactHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'contact-content.getCreate',
        'uses' => 'Admin\PageController@createContactContent'
    ]);
    Route::post('/create-content', [
        'as' => 'contact-content.postCreate',
        'uses' => 'Admin\PageController@storeContactContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'contact-header.getUpdate',
        'uses' => 'Admin\PageController@editContactHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'contact-header.postUpdate',
        'uses' => 'Admin\PageController@updateContactHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'contact-content.getUpdate',
        'uses' => 'Admin\PageController@editContactContent'
    ]);
    Route::post('/update-content', [
        'as' => 'contact-content.postUpdate',
        'uses' => 'Admin\PageController@updateContactContent'
    ]);
//            Delete
    Route::get('/contact/{id}', [
        'as' => 'contact.getDelete',
        'uses' => 'Admin\PageController@destroyContact'
    ]);
});


/**
 * Pages Imprint routes
 */
Route::group(['prefix' => 'imprint'], function () {
    Route::get('/', [
        'as' => 'imprint.getIndex',
        'uses' => 'Admin\PageController@indexImprint'
    ]);
    //    Create Img
    Route::get('/create-img',[
        'as' => 'imprint-img.getCreate',
        'uses' => 'Admin\PageController@createImprintImg'
    ]);
    Route::post('/create-img',[
        'as' => 'imprint-img.postCreate',
        'uses' => 'Admin\PageController@storeImprintImg'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'imprint-header.getCreate',
        'uses' => 'Admin\PageController@createImprintHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'imprint-header.postCreate',
        'uses' => 'Admin\PageController@storeImprintHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'imprint-content.getCreate',
        'uses' => 'Admin\PageController@createImprintContent'
    ]);
    Route::post('/create-content', [
        'as' => 'imprint-content.postCreate',
        'uses' => 'Admin\PageController@storeImprintContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'imprint-header.getUpdate',
        'uses' => 'Admin\PageController@editImprintHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'imprint-header.postUpdate',
        'uses' => 'Admin\PageController@updateImprintHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'imprint-content.getUpdate',
        'uses' => 'Admin\PageController@editImprintContent'
    ]);
    Route::post('/update-content', [
        'as' => 'imprint-content.postUpdate',
        'uses' => 'Admin\PageController@updateImprintContent'
    ]);
//            Delete
    Route::get('/imprint/{id}', [
        'as' => 'imprint.getDelete',
        'uses' => 'Admin\PageController@destroyImprint'
    ]);
});

/**
 * Pages Materials routes
 */
Route::group(['prefix' => 'materials'], function () {
    Route::get('/', [
        'as' => 'materials.getIndex',
        'uses' => 'Admin\PageController@indexMaterials'
    ]);
    //    Create Img
    Route::get('/create-img',[
        'as' => 'materials-img.getCreate',
        'uses' => 'Admin\PageController@createMaterialsImg'
    ]);
    Route::post('/create-img',[
        'as' => 'materials-img.postCreate',
        'uses' => 'Admin\PageController@storeMaterialsImg'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'materials-header.getCreate',
        'uses' => 'Admin\PageController@createMaterialsHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'materials-header.postCreate',
        'uses' => 'Admin\PageController@storeMaterialsHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'materials-content.getCreate',
        'uses' => 'Admin\PageController@createMaterialsContent'
    ]);
    Route::post('/create-content', [
        'as' => 'materials-content.postCreate',
        'uses' => 'Admin\PageController@storeMaterialsContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'materials-header.getUpdate',
        'uses' => 'Admin\PageController@editMaterialsHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'materials-header.postUpdate',
        'uses' => 'Admin\PageController@updateMaterialsHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'materials-content.getUpdate',
        'uses' => 'Admin\PageController@editMaterialsContent'
    ]);
    Route::post('/update-content', [
        'as' => 'materials-content.postUpdate',
        'uses' => 'Admin\PageController@updateMaterialsContent'
    ]);
//            Delete
    Route::get('/materials/{id}', [
        'as' => 'materials.getDelete',
        'uses' => 'Admin\PageController@destroyMaterials'
    ]);
});


/**
 * Pages Privacy Policy routes
 */
Route::group(['prefix' => 'privacy-policy'], function () {
    Route::get('/', [
        'as' => 'privacy-policy.getIndex',
        'uses' => 'Admin\PageController@indexPrivacyPolicy'
    ]);
    //    Create Img
    Route::get('/create-img',[
        'as' => 'privacy-policy-img.getCreate',
        'uses' => 'Admin\PageController@createPrivacyPolicyImg'
    ]);
    Route::post('/create-img',[
        'as' => 'privacy-policy-img.postCreate',
        'uses' => 'Admin\PageController@storePrivacyPolicyImg'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'privacy-policy-header.getCreate',
        'uses' => 'Admin\PageController@createPrivacyPolicyHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'privacy-policy-header.postCreate',
        'uses' => 'Admin\PageController@storePrivacyPolicyHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'privacy-policy-content.getCreate',
        'uses' => 'Admin\PageController@createPrivacyPolicyContent'
    ]);
    Route::post('/create-content', [
        'as' => 'privacy-policy-content.postCreate',
        'uses' => 'Admin\PageController@storePrivacyPolicyContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'privacy-policy-header.getUpdate',
        'uses' => 'Admin\PageController@editPrivacyPolicyHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'privacy-policy-header.postUpdate',
        'uses' => 'Admin\PageController@updatePrivacyPolicyHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'privacy-policy-content.getUpdate',
        'uses' => 'Admin\PageController@editPrivacyPolicyContent'
    ]);
    Route::post('/update-content', [
        'as' => 'privacy-policy-content.postUpdate',
        'uses' => 'Admin\PageController@updatePrivacyPolicyContent'
    ]);
//            Delete
    Route::get('/privacy-policy/{id}', [
        'as' => 'privacy-policy.getDelete',
        'uses' => 'Admin\PageController@destroyPrivacyPolicy'
    ]);
});

/**
 * Pages Main routes
 */
Route::group(['prefix' => 'main'], function () {
    Route::get('/', [
        'as' => 'main-admin.getIndex',
        'uses' => 'Admin\PageController@indexMain'
    ]);
    //Create Header
    Route::get('/create-header', [
        'as' => 'main-header.getCreate',
        'uses' => 'Admin\PageController@createMainHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'main-header.postCreate',
        'uses' => 'Admin\PageController@storeMainHeader'
    ]);
//            Create Content
    Route::get('/create-content', [
        'as' => 'main-content.getCreate',
        'uses' => 'Admin\PageController@createMainContent'
    ]);
    Route::post('/create-content', [
        'as' => 'main-content.postCreate',
        'uses' => 'Admin\PageController@storeMainContent'
    ]);
//            Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'main-header.getUpdate',
        'uses' => 'Admin\PageController@editMainHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'main-header.postUpdate',
        'uses' => 'Admin\PageController@updateMainHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'main-content.getUpdate',
        'uses' => 'Admin\PageController@editMainContent'
    ]);
    Route::post('/update-content', [
        'as' => 'main-content.postUpdate',
        'uses' => 'Admin\PageController@updateMainContent'
    ]);
//            Delete
    Route::get('/main/{id}', [
        'as' => 'main.getDelete',
        'uses' => 'Admin\PageController@destroyMain'
    ]);
});

Route::group(['prefix' => 'e-shop'], function () {
    Route::get('/', [
        'as' => 'e-shop.getIndex',
        'uses' => 'Admin\PageController@indexEShop'
    ]);
//    Create Img
    Route::get('/create-img',[
        'as' => 'e-shop-img.getCreate',
        'uses' => 'Admin\PageController@createEShopImg'
    ]);
    Route::post('/create-img',[
        'as' => 'e-shop-img.postCreate',
        'uses' => 'Admin\PageController@storeEShopImg'
    ]);
//    Create Header
    Route::get('/create-header', [
        'as' => 'e-shop-header.getCreate',
        'uses' => 'Admin\PageController@createEShopHeader'
    ]);
    Route::post('/create-header', [
        'as' => 'e-shop-header.postCreate',
        'uses' => 'Admin\PageController@storeEShopHeader'
    ]);
//    Create Content
    Route::get('/create-content', [
        'as' => 'e-shop-content.getCreate',
        'uses' => 'Admin\PageController@createEShopContent'
    ]);
    Route::post('/create-content', [
        'as' => 'e-shop-content.postCreate',
        'uses' => 'Admin\PageController@storeEShopContent'
    ]);
//    Edit Header
    Route::get('/update-header/{id}', [
        'as' => 'e-shop-header.getUpdate',
        'uses' => 'Admin\PageController@editEShopHeader'
    ]);
    Route::post('/update-header', [
        'as' => 'e-shop-header.postUpdate',
        'uses' => 'Admin\PageController@updateEShopHeader'
    ]);
//            Edit Content
    Route::get('/update-content/{id}', [
        'as' => 'e-shop-content.getUpdate',
        'uses' => 'Admin\PageController@editEShopContent'
    ]);
    Route::post('/update-content', [
        'as' => 'e-shop-content.postUpdate',
        'uses' => 'Admin\PageController@updateEShopContent'
    ]);
//            Delete
    Route::get('/e-shop/{id}', [
        'as' => 'e-shop.getDelete',
        'uses' => 'Admin\PageController@destroyEShop'
    ]);
});
