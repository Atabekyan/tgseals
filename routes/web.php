<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

require_once 'admin.php';
require_once 'main.php';


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


//Route::get('test/user', 'EShop\UserController@index');

Route::group(['middleware' => ['auth']], function () {

    Route::get('api/user', 'EShop\UserController@index');
    Route::get('api/product', 'EShop\ProductController@index');
    Route::get('api/order/check', 'EShop\ProductController@order');
    Route::get('api/basket/info', 'EShop\ProductController@basketIndex');
    Route::get('api/order/info', 'EShop\ProductController@orderIndex');


    Route::view('/e-shop/{path?}', 'main.e-shop.index')
        ->where('path', '.*')
        ->name('react');

});
