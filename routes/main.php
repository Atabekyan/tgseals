<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 27-Feb-19
 * Time: 4:07 PM
 */

Route::get('react' , function(){
    return view('main.e-shop.index');
});

Route::group(["middleware" => "locale"], function () {

    Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
    Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

    Route::get('storage/{filename}', function ($filename) {
        return Image::make(storage_path('public/' . $filename))->response();
    });

    Auth::routes();

    Route::post('/locale-change', [
        'as' => 'main.changeLocale',
        'uses' => 'Main\MainController@changeLocale']);

    Route::get('/', [
        'as' => 'main.getIndex',
        'uses' => 'Main\MainController@index']);

    Route::get('/products', [
        'as' => 'products.getProducts',
        'uses' => 'Main\ProductController@products']);

    Route::get('/products/{category_id}', [
        'as' => 'products.getProductList',
        'uses' => 'Main\ProductListController@productList']);

    Route::get('/contacts', [
        'as' => 'main.getContacts',
        'uses' => 'Main\ContactUSController@contacts']);

    Route::post('/contacts', [
        'as' => 'main.postContacts',
        'uses' => 'Main\ContactUSController@contactUSPost']);

//    Route::get('contact-us', 'ContactUSController@contacts');
//    Route::post('contact-us', ['as'=>'contactus.store','uses'=>'ContactUSController@contactUSPost']);

//    Route::get('/product-list', [
//        'as' => 'products.getProductList',
//        'uses' => 'Main\ProductListController@productList']);

    Route::get('/about-us', [
        'as' => 'main.getAboutUs',
        'uses' => 'Main\MainController@about_us']);

    Route::get('/materials', [
        'as' => 'main.getMaterials',
        'uses' => 'Main\MainController@materials']);

    Route::get('/applications', [
        'as' => 'main.getApplications',
        'uses' => 'Main\MainController@applications']);

    Route::get('/compounds', [
        'as' => 'main.getCompounds',
        'uses' => 'Main\MainController@compounds']);

    Route::get('/downloads', [
        'as' => 'main.getDownloads',
        'uses' => 'Main\MainController@downloads']);

    Route::get('/privacy-policy', [
        'as' => 'main.getPrivacyPolicy',
        'uses' => 'Main\MainController@privacy_policy']);

    Route::get('/imprint', [
        'as' => 'main.getImprint',
        'uses' => 'Main\MainController@imprint']);

    Route::group(['prefix' => 'e-shop'] , function(){
        Route::get('/info' , [
            'as'   => 'main.getEShop',
            'uses' => 'Main\MainController@e_shop_info'
        ]);

        Route::get('/' , [
            'as'   => 'main.getEShopIndex',
            'uses' => 'Main\MainController@e_shop']);
    });



});