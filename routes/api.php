<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


//Route::resource('user', 'EShop\UserController@index');

//Route::group(['middleware' => ['auth:api']], function() {

//    Route::get('user', 'EShop\UserController@index');
//     more private routes...
//});

Route::post('product/basket', 'EShop\ProductController@basket');
Route::post('product/order', 'EShop\ProductController@order');
Route::post('product/order/submit', 'EShop\ProductController@orderSubmit');
Route::post('user/edit', 'EShop\UserController@update');
Route::post('basket/delete_one', 'EShop\ProductController@deleteBasketOne');
Route::post('basket/delete_all', 'EShop\ProductController@deleteBasketAll');
Route::post('basket/add/order_one', 'EShop\ProductController@addBasketOne');
Route::post('basket/add/order_all', 'EShop\ProductController@addBasketOrderAll');



