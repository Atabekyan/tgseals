<?php
/**
 * Created by PhpStorm.
 * User: Artak Atabekyan
 * Date: 27-Feb-19
 * Time: 4:06 PM
 */

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::group(['middleware' => 'admin'], function () {

            require_once 'admin-pages.php';
            /**
             * dashboard routes
             */
            Route::get('/', [
                'as' => 'admin.getIndex',
                'uses' => 'Admin\DashboardController@index']);
            /**
             * Main Category routes
             */
            Route::group(['prefix' => 'main-category'], function () {
                Route::get('/', [
                    'as' => 'main-category.getIndex',
                    'uses' => 'Admin\CategoryController@index'
                ]);

                Route::get('/create', [
                    'as' => 'main-category.getCreate',
                    'uses' => 'Admin\CategoryController@createMainCategory'
                ]);

                Route::post('/create', [
                    'as' => 'main-category.postCreate',
                    'uses' => 'Admin\CategoryController@storeMainCategory'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'main-category.getUpdate',
                    'uses' => 'Admin\CategoryController@editMainCategory'
                ]);

                Route::post('/update', [
                    'as' => 'main-category.postUpdate',
                    'uses' => 'Admin\CategoryController@updateMainCategory'
                ]);
                Route::get('/main-category/{id}', [
                    'as' => 'main-category.getDelete',
                    'uses' => 'Admin\CategoryController@destroy'
                ]);
            });

            /**
             * Second Category routes
             */
            Route::group(['prefix' => 'second-category'], function () {
                Route::get('/', [
                    'as' => 'second-category.getIndex',
                    'uses' => 'Admin\CategoryController@indexSecondCategory'
                ]);

                Route::get('/create', [
                    'as' => 'second-category.getCreate',
                    'uses' => 'Admin\CategoryController@createSecondCategory'
                ]);

                Route::post('/create', [
                    'as' => 'second-category.postCreate',
                    'uses' => 'Admin\CategoryController@storeSecondCategory'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'second-category.getUpdate',
                    'uses' => 'Admin\CategoryController@editSecondCategory'
                ]);

                Route::post('/update', [
                    'as' => 'second-category.postUpdate',
                    'uses' => 'Admin\CategoryController@updateSecondCategory'
                ]);
                Route::get('/second-category/{id}', [
                    'as' => 'second-category.getDelete',
                    'uses' => 'Admin\CategoryController@destroy'
                ]);
            });

            /**
             * Product Category routes
             */
            Route::group(['prefix' => 'products'], function () {
                Route::get('/', [
                    'as' => 'product.getIndex',
                    'uses' => 'Admin\ProductController@index'
                ]);

                Route::get('/create', [
                    'as' => 'product.getCreate',
                    'uses' => 'Admin\ProductController@create'
                ]);

                Route::post('/create', [
                    'as' => 'product.postCreate',
                    'uses' => 'Admin\ProductController@store'
                ]);
                Route::get('/create-settings/{id}', [
                    'as' => 'product.getCreateSettings',
                    'uses' => 'Admin\ProductController@createSettings'
                ]);
                Route::post('/create-settings', [
                    'as' => 'product.postCreateSettings',
                    'uses' => 'Admin\ProductController@storeSettings'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'product.getUpdate',
                    'uses' => 'Admin\ProductController@edit'
                ]);

                Route::post('/update', [
                    'as' => 'product.postUpdate',
                    'uses' => 'Admin\ProductController@update'
                ]);

                Route::get('/update-settings/{id}', [
                    'as' => 'product.getUpdateSettings',
                    'uses' => 'Admin\ProductController@editSettings'
                ]);

                Route::post('/update-settings', [
                    'as' => 'product.postUpdateSettings',
                    'uses' => 'Admin\ProductController@updateSettings'
                ]);
                Route::get('/product/{id}', [
                    'as' => 'product.getDelete',
                    'uses' => 'Admin\ProductController@destroy'
                ]);
                Route::get('/product-settings/{id}', [
                    'as' => 'product.getDeleteSettings',
                    'uses' => 'Admin\ProductController@destroySettings'
                ]);
            });
            /**
             * User routes
             */
            Route::group(['prefix' => 'user'], function () {
                Route::get('/', [
                    'as' => 'user.getIndex',
                    'uses' => 'Admin\UserController@index'
                ]);

                Route::get('/create', [
                    'as' => 'user.getCreate',
                    'uses' => 'Admin\UserController@create'
                ]);

                Route::post('/create', [
                    'as' => 'user.postCreate',
                    'uses' => 'Admin\UserController@store'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'user.getUpdate',
                    'uses' => 'Admin\UserController@edit'
                ]);

                Route::post('/update', [
                    'as' => 'user.postUpdate',
                    'uses' => 'Admin\UserController@update'
                ]);
                Route::get('/user/{id}', [
                    'as' => 'user.getDelete',
                    'uses' => 'Admin\UserController@destroy'
                ]);
            });

            /**
             * User routes
             */
            Route::group(['prefix' => 'messages'], function () {
                Route::get('/', [
                    'as' => 'messages.getIndex',
                    'uses' => 'Admin\MessagesController@index'
                ]);

                Route::get('/messages/{id}', [
                    'as' => 'messages.getDelete',
                    'uses' => 'Admin\MessagesController@destroy'
                ]);
            });

            Route::group(['prefix' => 'store'], function () {

                Route::get('/export', 'Admin\StoreController@export')->name('export');
                Route::get('/importExportView', [
                    'as' => 'store.storeForSearch',
                    'uses' => 'Admin\StoreController@importExportView']);
                Route::post('/import', 'Admin\StoreController@import')->name('import');

                Route::get('/export_product', 'Admin\StoreController@exportProduct')->name('export.product');
                Route::get('/importExportViewProduct', [
                    'as' => 'store.storeProduct',
                    'uses' => 'Admin\StoreController@importExportViewProduct']);
                Route::post('/import_product', 'Admin\StoreController@importProduct')->name('import.product');

            });

            /**
             * Basket routes start
             */
            Route::group(['prefix' => 'basket'], function () {
                Route::get('/', [
                    'as' => 'basket.getIndex',
                    'uses' => 'Admin\BasketController@index'
                ]);

                Route::get('/create', [
                    'as' => 'basket.getCreate',
                    'uses' => 'Admin\BasketController@create'
                ]);

                Route::post('/create', [
                    'as' => 'basket.postCreate',
                    'uses' => 'Admin\BasketController@store'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'basket.getUpdate',
                    'uses' => 'Admin\BasketController@edit'
                ]);

                Route::post('/update', [
                    'as' => 'basket.postUpdate',
                    'uses' => 'Admin\BasketController@update'
                ]);
                Route::get('/user/{id}', [
                    'as' => 'basket.getDelete',
                    'uses' => 'Admin\BasketController@destroy'
                ]);
            });

            /**
             * Basket routes end
             */

            /**
             * Order routes start
             */
            Route::group(['prefix' => 'order'], function () {
                Route::get('/', [
                    'as' => 'order.getIndex',
                    'uses' => 'Admin\OrderController@index'
                ]);

                Route::get('/create', [
                    'as' => 'order.getCreate',
                    'uses' => 'Admin\OrderController@create'
                ]);

                Route::post('/create', [
                    'as' => 'order.postCreate',
                    'uses' => 'Admin\OrderController@store'
                ]);

                Route::get('/update/{id}', [
                    'as' => 'order.getUpdate',
                    'uses' => 'Admin\OrderController@edit'
                ]);

                Route::post('/update', [
                    'as' => 'order.postUpdate',
                    'uses' => 'Admin\OrderController@update'
                ]);
                Route::get('/user/{id}', [
                    'as' => 'order.getDelete',
                    'uses' => 'Admin\OrderController@destroy'
                ]);
            });

            /**
             * User routes
             */
        });
    });
});


